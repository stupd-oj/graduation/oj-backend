import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AmqpConfig } from './amqp.config';

@Module({
  imports: [ConfigModule],
  providers: [AmqpConfig],
  exports: [AmqpConfig],
})
export class AmqpConfigModule {}
