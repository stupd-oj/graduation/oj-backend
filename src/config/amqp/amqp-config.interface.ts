export interface IAmqpConfig {
  readonly protocol?: string;
  readonly host?: string;
  readonly port?: number;
  readonly user?: string;
  readonly password?: string;
  readonly vhost?: string;
  readonly uri?: string;
}
