import { IAmqpConfig } from './amqp-config.interface';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AmqpConfig implements IAmqpConfig {
  readonly protocol: string;
  readonly host: string;
  readonly password: string;
  readonly port: number;
  readonly uri: string;
  readonly user: string;
  readonly vhost: string;

  constructor(configService: ConfigService) {
    try {
      this.uri = configService.getOrThrow(Environment.RABBITMQ_URI);
      const uri = new URL(this.uri);
      this.host = uri.hostname;
      this.port = Number(uri.port);
      this.user = uri.username;
      this.password = uri.password;
      this.vhost = uri.pathname;
      this.protocol = uri.protocol.replace(':', '');
    } catch (e) {
      this.host = configService.getOrThrow(Environment.RABBITMQ_HOST);
      this.port = configService.getOrThrow(Environment.RABBITMQ_PORT);
      this.user = configService.getOrThrow(Environment.RABBITMQ_USER);
      this.password = configService.getOrThrow(Environment.RABBITMQ_PASSWORD);
      this.vhost = configService.getOrThrow(Environment.RABBITMQ_VHOST);
      this.protocol = configService.get(Environment.RABBITMQ_PROTOCOL, 'amqp');
      this.uri = `${this.protocol}://${this.user}:${this.password}@${this.host}:${this.port}/${this.vhost}`;
    }
  }
}
