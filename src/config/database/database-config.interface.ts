import { EntitySchema } from 'typeorm';
import { Logger } from 'typeorm/logger/Logger';
import { MixedList } from 'typeorm/browser';

interface IBaseDatabaseConfig {
  readonly type: string;
  readonly host?: string;
  readonly port?: number;
  readonly username?: string;
  readonly password?: string;
  readonly database?: string;
  readonly entities?: (string | EntitySchema | any)[];
  readonly migrations?: MixedList<Function | string>;
  readonly logger?: Logger;
  readonly synchronize?: boolean;
  readonly autoLoadEntities?: boolean;
  readonly url?: string;
  readonly name?: string;
}

export interface IPostgresDatabaseConfig extends IBaseDatabaseConfig {
  type: 'postgres';
}
