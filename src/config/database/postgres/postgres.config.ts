import { IPostgresDatabaseConfig } from '../database-config.interface';
import { ConfigService } from '@nestjs/config';
import { EntitySchema, Logger } from 'typeorm';
import { Environment } from '../../../common/constants/environment';
import { postgresConfig } from './configuration';
import { Injectable } from '@nestjs/common';
import { MixedList } from 'typeorm/browser';

@Injectable()
export class PostgresConfig implements IPostgresDatabaseConfig {
  readonly type = 'postgres';
  readonly database: string;
  readonly entities: (string | EntitySchema)[];
  readonly host: string;
  readonly logger: Logger;
  readonly migrations: MixedList<Function | string>;
  readonly password: string;
  readonly port: number;
  readonly synchronize: boolean;
  readonly username: string;
  readonly autoLoadEntities: boolean;
  readonly name: string;

  constructor(configService: ConfigService) {
    this.host = configService.get(Environment.DB_HOST);
    this.port = configService.get<number>(Environment.DB_PORT);
    this.username = configService.get(Environment.DB_USER);
    this.password = configService.get(Environment.DB_PASSWORD);
    this.database = configService.get(Environment.DB_NAME);
    this.entities = postgresConfig.entities;
    this.migrations = postgresConfig.migrations;
    this.synchronize = postgresConfig.synchronize;
    this.autoLoadEntities = postgresConfig.autoLoadEntities;
  }
}
