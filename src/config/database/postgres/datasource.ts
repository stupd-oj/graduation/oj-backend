import 'dotenv/config';
import { DataSource } from 'typeorm';
import { postgresConfig } from './configuration';

export default new DataSource(postgresConfig);
