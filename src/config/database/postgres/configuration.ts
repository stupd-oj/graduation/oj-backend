import 'dotenv/config';
import { IPostgresDatabaseConfig } from '../database-config.interface';
import { Environment } from '../../../common/constants/environment';
import ENTITIES from '../../../entities/sql';
import MIGRATIONS from '../../../migrations';

export const postgresConfig: IPostgresDatabaseConfig = {
  type: 'postgres',
  host: process.env[Environment.DB_HOST],
  port: Number(process.env[Environment.DB_PORT]),
  username: process.env[Environment.DB_USER],
  password: process.env[Environment.DB_PASSWORD],
  database: process.env[Environment.DB_NAME],
  entities: ENTITIES,
  autoLoadEntities: true,
  migrations: MIGRATIONS,
  synchronize: true,
};
