import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PostgresConfig } from './postgres.config';

@Module({
  imports: [ConfigModule],
  providers: [PostgresConfig],
  exports: [PostgresConfig],
})
export class PostgresConfigModule {}
