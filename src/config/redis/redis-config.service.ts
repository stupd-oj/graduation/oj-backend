import { IRedisConfig } from './redis-config.interface';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RedisConfig implements IRedisConfig {
  readonly database: number;
  readonly host: string;
  readonly username: string;
  readonly password: string;
  readonly port: number;
  readonly url: string;

  constructor(configService: ConfigService) {
    this.host = configService.getOrThrow(Environment.REDIS_HOST);
    this.port = configService.getOrThrow(Environment.REDIS_PORT);
    this.username = configService.get(Environment.REDIS_USERNAME, 'default');
    this.password = configService.getOrThrow(Environment.REDIS_PASSWORD);
    this.database = configService.get(Environment.REDIS_DATABASE, 0);
    this.url = `redis://${this.username}:${this.password}@${this.host}:${this.port}`;
  }
}
