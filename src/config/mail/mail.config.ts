import { IMailConfig } from './mail-config.interface';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailConfig implements IMailConfig {
  from: string;
  host: string;
  password: string;
  port: number;
  user: string;
  name: string;

  constructor(configService: ConfigService) {
    this.port = configService.get(Environment.MAIL_PORT);
    this.host = configService.get(Environment.MAIL_HOST);
    this.password = configService.get(Environment.MAIL_PASSWORD);
    this.user = configService.get(Environment.MAIL_USER);
    this.name = configService.get(Environment.MAIL_NAME);
    this.from = `${this.name} <${this.user}>`;
  }
}
