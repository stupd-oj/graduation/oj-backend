export interface IS3Config {
  readonly publicEndpoint: string;
  readonly endPoint: string;
  readonly accessKeyId: string;
  readonly secretAccessKey: string;
  readonly region: string;
  readonly bucketName: string;
}
