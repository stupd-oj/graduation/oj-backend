import { IS3Config } from './s3-config.interface';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';
import { Injectable } from '@nestjs/common';
import * as process from 'process';

@Injectable()
export class S3Config implements IS3Config {
  static readonly PUBLIC_ACCESS_POINT =
    process.env[Environment.S3_PUBLIC_ENDPOINT] ?? '';
  readonly accessKeyId: string;
  readonly bucketName: string;
  readonly endPoint: string;
  readonly publicEndpoint: string;
  readonly secretAccessKey: string;
  readonly region: string;

  constructor(configService: ConfigService) {
    this.endPoint = configService.get<string>(Environment.S3_ENDPOINT);
    this.publicEndpoint = configService.get<string>(
      Environment.S3_PUBLIC_ENDPOINT,
      this.endPoint,
    );
    this.accessKeyId = configService.get<string>(Environment.S3_ACCESS_KEY_ID);
    this.secretAccessKey = configService.get<string>(
      Environment.S3_SECRET_ACCESS_KEY,
    );
    this.bucketName = configService.get<string>(Environment.S3_BUCKET_NAME);
    this.region = configService.get<string>(Environment.S3_REGION, 'auto');
  }
}
