import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { S3Config } from './s3.config';

@Module({
  imports: [ConfigModule],
  providers: [S3Config],
  exports: [S3Config],
})
export class S3ConfigModule {}
