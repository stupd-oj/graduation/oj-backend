export interface IJwtConfig {
  secret?: string;
  issuer?: string;
  expiration?: string;
}
