import { Injectable } from '@nestjs/common';
import { IJwtConfig } from './jwt-config.interface';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';

@Injectable()
export class JwtConfig implements IJwtConfig {
  expiration: string;
  issuer: string;
  secret: string;

  constructor(configService: ConfigService) {
    this.expiration = configService.get(Environment.JWT_EXPIRATION);
    this.issuer = configService.get(Environment.JWT_ISSUER);
    this.secret = configService.get(Environment.JWT_SECRET);
  }
}
