import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtConfig } from './jwt.config';

@Module({
  imports: [ConfigModule],
  providers: [JwtConfig],
  exports: [JwtConfig],
})
export class JwtConfigModule {}
