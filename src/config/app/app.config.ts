import { IAppConfig } from './app-config.interface';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Environment } from '../../common/constants/environment';
import { appConfig } from './configuration';

@Injectable()
export class AppConfig implements IAppConfig {
  readonly environment: string;
  readonly apiPrefix: string;
  readonly apiVersion: string;
  readonly clientUrl: string;
  readonly isProduction: boolean;
  readonly logMaxAge: string;
  readonly logMaxSize: string;
  readonly port: number;
  readonly timeZone: string;

  constructor(configService: ConfigService) {
    this.isProduction = appConfig.isProduction;
    this.environment = appConfig.environment;
    this.apiPrefix = configService.get(Environment.SERVER_API_PREFIX);
    this.apiVersion = configService.get(Environment.SERVER_API_VERSION);
    this.clientUrl = configService.get(Environment.CLIENT_URL);
    this.logMaxSize = configService.get(Environment.LOGGER_MAX_FILE_SIZE);
    this.logMaxAge = configService.get(Environment.LOGGER_MAX_AGE);
    this.timeZone = configService.get(Environment.TIME_ZONE);
    this.port = configService.getOrThrow(Environment.SERVER_PORT, 8081);
  }
}
