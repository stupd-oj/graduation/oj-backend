export interface IAppConfig {
  readonly environment: string;
  readonly isProduction: boolean;
  readonly port: number;
  readonly apiPrefix?: string;
  readonly apiVersion?: string;
  readonly clientUrl?: string;
  readonly logMaxAge?: string;
  readonly logMaxSize?: string;
  readonly timeZone?: string;
}
