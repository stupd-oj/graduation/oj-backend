import 'dotenv/config';
import { Environment } from '../../common/constants/environment';
import { IAppConfig } from './app-config.interface';

export const appConfig: Partial<IAppConfig> = {
  environment: process.env[Environment.NODE_ENV] ?? 'development',
  isProduction: process.env[Environment.NODE_ENV] === 'production',
  port: Number(process.env[Environment.SERVER_PORT]),
  timeZone: process.env[Environment.TIME_ZONE],
  logMaxAge: process.env[Environment.LOGGER_MAX_AGE],
  logMaxSize: process.env[Environment.LOGGER_MAX_FILE_SIZE],
};
