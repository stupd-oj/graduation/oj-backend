import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APILoggerMiddleware } from './common/middlewares/api-logger.middleware';
import { AppConfigModule } from './config/app/app-config.module';
import { RedisConfigModule } from './config/redis/redis-config.module';
import { PostgresProviderModule } from './providers/database/postgres/provider.module';
import { MailProviderModule } from './providers/mail/provider.module';
import { QueueProviderModule } from './providers/queue/provider.module';
import { CacheProviderModule } from './providers/cache/provider.module';
import { RedisProviderModule } from './providers/redis/provider.module';
import { AmqpConfigModule } from './config/amqp/amqp-config.module';
import { AmqpProviderModule } from './providers/amqp/provider.module';
import { AuthModule } from './pkg/authentication/auth.module';
import { UserModule } from './pkg/user/user.module';
import { ContestModule } from './pkg/contest/contest.module';
import { ProblemModule } from './pkg/problem/problem.module';
import { SubmissionModule } from './pkg/submission/submission.module';
import { EventProviderModule } from './providers/event/provider.module';
import { RankingModule } from './pkg/ranking/ranking.module';
import { ScheduleProviderModule } from './providers/schedule/provider.module';
import { FileModule } from './pkg/file/file.module';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { StatisticModule } from './pkg/statistic/statistic.module';

const SHARED_MODULES = [
  ConfigModule.forRoot({ envFilePath: ['.env'] }),
  PostgresProviderModule,
  MailProviderModule,
  QueueProviderModule,
  CacheProviderModule,
  RedisProviderModule,
  AmqpProviderModule,
  EventProviderModule,
  ScheduleProviderModule,
  TerminusModule,
];

const CONFIG_MODULES = [AppConfigModule, RedisConfigModule, AmqpConfigModule];

@Module({
  imports: [
    ...SHARED_MODULES,
    ...CONFIG_MODULES,
    AuthModule,
    UserModule,
    ContestModule,
    ProblemModule,
    SubmissionModule,
    RankingModule,
    FileModule,
    StatisticModule,
  ],
  controllers: [HealthController],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(APILoggerMiddleware).forRoutes('*');
  }
}
