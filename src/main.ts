import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import { JwtAuthGuard } from './common/guards/jwt-auth.guard';
import { RoleGuard } from './common/guards/role.guard';
import { HttpExceptionFilter } from './common/exceptions/http-exeption.filter';
import { IAppConfig } from './config/app/app-config.interface';
import { AppConfig } from './config/app/app.config';
import {
  initAPIDocs,
  initMicroservices,
} from './common/helpers/bootstrap.helper';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  });
  const logger = new Logger('Bootstrap');
  const appConfig: IAppConfig = app.get(AppConfig);
  app.setGlobalPrefix(appConfig.apiPrefix, { exclude: ['health'] });
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: appConfig.apiVersion,
  });
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(new Reflector()));
  app.useGlobalGuards(
    new JwtAuthGuard(new Reflector()),
    new RoleGuard(new Reflector()),
  );
  initAPIDocs(app);
  await initMicroservices(app);

  await app.listen(appConfig.port);
  logger.log(`Server is listening at port ${appConfig.port}`);
}

bootstrap().catch(console.error);
