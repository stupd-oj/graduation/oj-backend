export enum Event {
  SUBMISSION_CREATED = 'submission:create',
  SUBMISSION_UPDATED = 'submission:update',
  SUBMISSION_RESULT_UPDATED = 'submission:update_result',
  USER_VERIFIED = 'user:verified',
}
