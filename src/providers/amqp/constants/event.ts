export enum Event {
  NEW_JUDGE_TASK = 'judge_task:new',
  UPDATE_JUDGE_TASK = 'judge_task:update',
}
