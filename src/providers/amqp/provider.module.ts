import { Global, Module } from '@nestjs/common';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { AmqpConfigModule } from '../../config/amqp/amqp-config.module';
import { AmqpConfig } from '../../config/amqp/amqp.config';
import { IAmqpConfig } from '../../config/amqp/amqp-config.interface';
import { AmqpQueue } from './constants/queue';

@Global()
@Module({
  imports: [AmqpConfigModule],
  providers: [
    {
      inject: [AmqpConfig],
      provide: AmqpQueue.JUDGER_SERVICE,
      useFactory: (amqpConfig: IAmqpConfig) => {
        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            urls: [amqpConfig.uri],
            queue: AmqpQueue.JUDGER_SERVICE,
          },
        });
      },
    },
  ],
  exports: [AmqpQueue.JUDGER_SERVICE],
})
export class AmqpProviderModule {}
