import { Global, Module } from '@nestjs/common';
import { RedisService, RedisServiceImpl } from './redis.service';
import { RedisConfigModule } from '../../config/redis/redis-config.module';

@Global()
@Module({
  imports: [RedisConfigModule],
  providers: [
    {
      provide: RedisService,
      useClass: RedisServiceImpl,
    },
  ],
  exports: [RedisService],
})
export class RedisProviderModule {}
