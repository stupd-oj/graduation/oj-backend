import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { RedisClientType } from 'redis';
import { RedisConfig } from '../../config/redis/redis-config.service';
import { IRedisConfig } from '../../config/redis/redis-config.interface';
import { createRedisClient } from '../../common/helpers/redis.helper';

export abstract class RedisService {
  abstract get<T extends any>(key: string): Promise<any>;
  abstract set(key: string, value: any, ttl?: number): Promise<void>;
  abstract del(key: string): Promise<void>;
  abstract getClient(): RedisClientType;
}

@Injectable()
export class RedisServiceImpl extends RedisService implements OnModuleInit {
  private readonly logger = new Logger(RedisService.name);
  private readonly client: RedisClientType;
  private readonly keyPrefix: string = '_server@';

  constructor(@Inject(RedisConfig) redisConfig: IRedisConfig) {
    super();
    this.client = createRedisClient(redisConfig);
  }

  private keySerialize = (key: string) => `${this.keyPrefix}${key}`;

  async onModuleInit() {
    try {
      await this.client.connect();
    } catch (e) {
      this.logger.error('Redis connect error', e);
    }
  }

  async get<T extends any>(key: string): Promise<T> {
    const value = await this.client.get(this.keySerialize(key));
    try {
      return JSON.parse(value);
    } catch (e) {
      return value as any;
    }
  }

  async set(key: string, value: any, ttl = 0): Promise<void> {
    await this.client.set(this.keySerialize(key), JSON.stringify(value), {
      EX: ttl,
    });
  }

  async del(key: string): Promise<void> {
    await this.client.del(this.keySerialize(key));
  }

  getClient(): RedisClientType {
    return this.client;
  }
}
