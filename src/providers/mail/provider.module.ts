import { Global, Module } from '@nestjs/common';
import { MailConfigModule } from '../../config/mail/mail-config.module';
import { MailerModule, MailerOptions } from '@nestjs-modules/mailer';
import { MailConfig } from '../../config/mail/mail.config';
import { IMailConfig } from '../../config/mail/mail-config.interface';
import * as path from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

@Global()
@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [MailConfigModule],
      inject: [MailConfig],
      useFactory: (mailConfig: IMailConfig): MailerOptions => ({
        transport: {
          host: mailConfig.host,
          port: mailConfig.port,
          secure: false,
          auth: {
            user: mailConfig.user,
            pass: mailConfig.password,
          },
          secureConnection: false,
          requireTLS: true,
          tls: {
            ciphers: 'SSLv3',
          },
        },
        defaults: {
          from: mailConfig.from,
        },
        template: {
          dir: process['pkg']
            ? path.join(__dirname, '..', 'templates/mail')
            : path.join(process.cwd(), 'templates/mail'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: false,
          },
        },
      }),
    }),
  ],
})
export class MailProviderModule {}
