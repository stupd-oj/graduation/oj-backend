export interface PasswordRecoverEmail {
  email: string;
  name: string;
  code: string;
}
