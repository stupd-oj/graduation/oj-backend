export interface AccountVerificationEmail {
  email: string;
  name: string;
  code: string;
}
