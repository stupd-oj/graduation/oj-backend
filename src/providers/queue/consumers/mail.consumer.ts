import { OnQueueError, Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { MailJob } from '../constants/job';
import { Job } from 'bull';
import { BullQueue } from '../constants/queue';
import { MailerService } from '@nestjs-modules/mailer';
import { AppConfig } from '../../../config/app/app.config';
import { AccountVerificationEmail } from '../../mail/interfaces/account-verification.interface';
import { PasswordRecoverEmail } from '../../mail/interfaces/password-recover.interface';

@Processor(BullQueue.mail)
export class MailConsumer {
  private readonly logger = new Logger(MailConsumer.name);
  constructor(
    private readonly mailerService: MailerService,
    private readonly appConfig: AppConfig,
  ) {}

  @OnQueueError()
  onQueueError(error: Error) {
    this.logger.error(error);
  }

  @Process(MailJob.ACCOUNT_VERIFICATION)
  async onSendAccountVerificationEmail(job: Job<AccountVerificationEmail>) {
    try {
      const { data } = job;
      const { email, code, name } = data ?? {};
      const dataEncoded = Buffer.from(`${email}::${code}`).toString(
        'base64url',
      );
      await this.mailerService.sendMail({
        to: data.email,
        subject: 'Account Verification',
        template: 'account-verification',
        context: {
          name,
          code,
          link: `${this.appConfig.clientUrl}/auth/account-verification?t=${dataEncoded}`,
        },
      });
    } catch (e) {
      this.logger.error(e);
    }
  }

  @Process(MailJob.PASSWORD_RECOVER)
  async onSendPasswordRecoverEmail(job: Job<PasswordRecoverEmail>) {
    try {
      const { data } = job;
      const { email, code, name } = data ?? {};
      const dataEncoded = Buffer.from(`${email}::${code}`).toString(
        'base64url',
      );
      await this.mailerService.sendMail({
        to: data.email,
        subject: 'Password Forgotten',
        template: 'password-recover',
        context: {
          name,
          code,
          link: `${this.appConfig.clientUrl}/auth/reset-password?t=${dataEncoded}`,
        },
      });
    } catch (e) {
      this.logger.error(e);
    }
  }
}
