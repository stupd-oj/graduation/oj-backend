import { OnQueueError, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { BullQueue } from '../constants/queue';

@Processor(BullQueue.default)
export class DefaultConsumer {
  private readonly logger = new Logger(DefaultConsumer.name);
  constructor() {}

  @OnQueueError()
  onQueueError(error: Error) {
    this.logger.error(error);
  }
}
