import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { BullQueue } from '../constants/queue';
import { Queue } from 'bull';

export abstract class MailProducer {
  abstract send<T>(pattern: string, data: T): Promise<void>;
}

@Injectable()
export class MailProducerImpl extends MailProducer {
  constructor(
    @InjectQueue(BullQueue.mail)
    private readonly mailQueue: Queue,
  ) {
    super();
  }

  async send<T>(pattern: string, data: T): Promise<void> {
    await this.mailQueue.add(pattern, data);
  }
}
