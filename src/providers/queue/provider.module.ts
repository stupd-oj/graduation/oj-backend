import { Global, Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';
import { QueueOptions } from 'bull';
import { RedisConfigModule } from '../../config/redis/redis-config.module';
import { RedisConfig } from '../../config/redis/redis-config.service';
import { IRedisConfig } from '../../config/redis/redis-config.interface';
import { BullQueue } from './constants/queue';
import { MailProducer, MailProducerImpl } from './producers/mail.producer';
import { MailConsumer } from './consumers/mail.consumer';
import { DefaultConsumer } from './consumers/default.consumer';

@Global()
@Module({
  imports: [
    BullModule.forRootAsync({
      imports: [RedisConfigModule],
      inject: [RedisConfig],
      useFactory: (redisConfig: IRedisConfig): QueueOptions => ({
        redis: {
          host: redisConfig.host,
          port: redisConfig.port,
          keyPrefix: '@queue',
        },
      }),
    }),
    BullModule.registerQueue({ name: BullQueue.default }),
    BullModule.registerQueue({ name: BullQueue.mail }),
  ],
  providers: [
    DefaultConsumer,
    MailConsumer,
    {
      provide: MailProducer,
      useClass: MailProducerImpl,
    },
  ],
  exports: [MailProducer],
})
export class QueueProviderModule {}
