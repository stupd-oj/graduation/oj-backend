import { getDataSourceToken } from '@nestjs/typeorm';

export const DataSourceToken = Object.freeze({
  primary: getDataSourceToken(),
});
