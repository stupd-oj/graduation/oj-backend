import { Module } from '@nestjs/common';
import { PostgresConfigModule } from '../../../config/database/postgres/postgres-config.module';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { PostgresConfig } from '../../../config/database/postgres/postgres.config';
import { IPostgresDatabaseConfig } from '../../../config/database/database-config.interface';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [PostgresConfigModule],
      inject: [PostgresConfig],
      useFactory: (
        postgresConfig: IPostgresDatabaseConfig,
      ): TypeOrmModuleOptions => postgresConfig,
    }),
  ],
})
export class PostgresProviderModule {}
