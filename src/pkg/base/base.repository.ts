import { FindOneOptions, Repository } from 'typeorm';

export interface IBaseRepository<Entity> extends Repository<Entity> {}

export interface CanGetById<Entity> {
  findById(id, options?: FindOneOptions<Entity>): Promise<Entity>;
  findByIdOrFail(id, options?: FindOneOptions<Entity>): Promise<Entity>;
}
