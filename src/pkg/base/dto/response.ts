export type Response<T = null> = Promise<ResponseDTO<T>>;

export class ResponseDTO<T = null> {
  message: string;
  data: T;
  meta: {
    total?: number;
    [props: string]: unknown;
  };
  errorCode: string;
  timestamp: number;

  constructor(partial: Partial<ResponseDTO<T>>) {
    Object.assign(this, partial);
  }

  public static ok<T>(
    data?: T,
    total?: number,
    meta?: Record<string, unknown>,
  ): ResponseDTO<T> {
    return new ResponseDTO<T>({
      message: 'Success',
      data,
      meta: {
        ...meta,
        total,
      },
      timestamp: Date.now(),
    });
  }

  public static error(err: any, meta?: Record<string, unknown>): ResponseDTO {
    return new ResponseDTO({
      message: err?.message ?? err,
      errorCode: err?.code,
      meta: meta,
      timestamp: Date.now(),
    });
  }
}
