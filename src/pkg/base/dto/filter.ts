import { IsInt, IsOptional, IsString } from "class-validator";
import { Number } from '../../../common/decorators/serializers/number.decorator';
import { SortField } from '../../../common/decorators/serializers/sort-field.decorator';
import { IsNumber } from '../../../common/decorators/validations/is-number.decorator';
import { Sort } from './sort';
import { Default } from '../../../common/decorators/serializers/default.decorator';
import { AcceptedSortField } from '../../../common/decorators/validations/accepted-sort-field.decorator';

export class BaseFilter {
  @IsOptional()
  @Number({ toClassOnly: true })
  @IsNumber({
    min: 0,
  })
  @IsInt()
  offset: number = 0;

  @IsOptional()
  @Number({ toClassOnly: true })
  @IsNumber({
    min: 0,
    max: 100,
  })
  @IsInt()
  limit: number = 20;

  @AcceptedSortField()
  @SortField()
  @Default([])
  sort: Array<Sort>;

  @IsOptional()
  @IsString()
  filter: string;

  @IsOptional()
  @IsString()
  query: string;

  @IsOptional()
  @IsString()
  select: string;
}
