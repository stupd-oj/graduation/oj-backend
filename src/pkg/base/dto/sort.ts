import { IsIn, IsString } from 'class-validator';

export class Sort {
  @IsString()
  field: string;

  @IsString()
  @IsIn(['DESC', 'ASC'])
  direction: 'DESC' | 'ASC';
}
