import { Inject, Injectable } from '@nestjs/common';
import { Response, ResponseDTO } from '../base/dto/response';
import { HttpException } from '../../common/exceptions/http.exception';
import { PreDefinedError } from '../../common/constants/pre-defined-error';
import { UserFilter } from './dto/user-filter';
import { UserRepository } from './repositories/user.repository';
import { ProfileRepository } from './repositories/profile.repository';
import { IUser, IUserRepository } from './interfaces/user.interface';
import { IProfileRepository } from './interfaces/profile.interface';
import { User } from './serializers/user.serializer';
import { isNil } from 'lodash';
import { Profile } from './serializers/profile.serializer';

export abstract class UserService {
  abstract authenticate(identifier: string, password: string): Promise<User>;
  abstract filter(filter: UserFilter): Response<User[]>;
  abstract create(
    partial: Partial<IUser> & { password: string },
  ): Promise<User>;
  abstract update(id: number, partial: Partial<User>): Promise<void>;
  abstract findByIdOrFail(id: number): Promise<User>;
  abstract findByIdentifierOrFail(identifier: string): Promise<User>;
  abstract checkIdentifierAvailability(identifier: string): Promise<boolean>;
  abstract getProfileByUsername(username: string): Response<Profile>;
  abstract updatePassword(userId: number, newPassword: string): Promise<void>;
  abstract updatePassword(
    userId: number,
    oldPassword: string,
    newPassword: string,
  ): Promise<void>;
}

@Injectable()
export class UserServiceImpl extends UserService {
  constructor(
    @Inject(ProfileRepository.provide)
    private readonly profileRepository: IProfileRepository,
    @Inject(UserRepository.provide)
    private readonly userRepository: IUserRepository,
  ) {
    super();
  }

  async authenticate(identifier: string, password: string): Promise<User> {
    const user = await this.userRepository.findByIdentifier(identifier);
    if (!(user && this.userRepository.comparePassword(user, password))) {
      throw HttpException.badRequest(PreDefinedError.AUTH_INCORRECT);
    }
    return this.userRepository.toSerializer(user);
  }

  async filter(filter: UserFilter): Promise<ResponseDTO<User[]>> {
    const { query = '', offset, limit, sort = [] } = filter;
    const queryBuilder = this.userRepository.createQueryBuilder('u');

    if (query) {
      queryBuilder.where(
        'u.username LIKE :query OR u.full_name ILIKE :query OR u.email ILIKE :query',
        { query: `%${query}%` },
      );
    }

    queryBuilder.limit(limit);
    queryBuilder.offset(offset);
    for (const s of sort) {
      queryBuilder.orderBy(s.field, s.direction);
    }

    const [entities, total] = await queryBuilder.getManyAndCount();
    return ResponseDTO.ok(
      this.userRepository.toSerializerMany(entities),
      total,
    );
  }

  async create(partial: Partial<User> & { password: string }): Promise<User> {
    const user = this.userRepository.create(partial);
    await this.userRepository.save(user);
    await this.userRepository.updatePassword(user, partial.password);
    return this.userRepository.toSerializer(user);
  }

  async findByIdOrFail(id: number): Promise<User> {
    return this.userRepository
      .findByIdOrFail(id)
      .then((e) => this.userRepository.toSerializer(e));
  }

  async findByIdentifierOrFail(identifier: string): Promise<User> {
    return this.userRepository
      .findByIdentifierOrFail(identifier)
      .then(this.userRepository.toSerializer);
  }

  async checkIdentifierAvailability(identifier: string): Promise<boolean> {
    return !(await this.userRepository.findByIdentifier(identifier));
  }

  async getProfileByUsername(username: string): Response<Profile> {
    const profile = await this.profileRepository
      .findOneByOrFail({ username })
      .catch(() => {
        throw HttpException.notFound(PreDefinedError.USER_NOT_FOUND);
      });
    profile.contact = await this.userRepository.findById(profile.id);
    return ResponseDTO.ok(this.profileRepository.toSerializer(profile));
  }

  async update(id: number, partial: Partial<User>): Promise<void> {
    const user = await this.userRepository.findByIdOrFail(id);
    this.userRepository.merge(user, partial);
    await this.userRepository.save(user);
  }

  async updatePassword(
    userId: number,
    oldPassword: string,
    newPassword?: string,
  ): Promise<void> {
    if (isNil(newPassword)) {
      newPassword = oldPassword;
      oldPassword = null;
    }

    const user = await this.userRepository.findByIdOrFail(userId);
    if (
      !isNil(oldPassword) &&
      !this.userRepository.comparePassword(user, oldPassword)
    )
      throw HttpException.badRequest(PreDefinedError.OLD_PASSWORD_INCORRECT);
    await this.userRepository.updatePassword(user, newPassword);
  }
}
