import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  SerializeOptions,
} from '@nestjs/common';
import { UserService } from '../user.service';
import { Role } from '../../../common/decorators/metadata/role.decorator';
import { UserRole } from '../constants/user-role';
import { UserFilter } from '../dto/user-filter';
import { Response, ResponseDTO } from '../../base/dto/response';
import { IUser } from '../interfaces/user.interface';
import { AdminUpdateUserDTO } from '../dto/update-user.dto';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { BanUserDTO } from '../dto/ban-user.dto';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { SerialGroup } from '../../../common/constants/serial-group';

@ApiTags(API_TAG.MANAGE_USER)
@Controller('admin/users')
@Role(UserRole.Admin)
@SerializeOptions({ groups: [SerialGroup.Admin] })
export class ManageUserController {
  constructor(private readonly userService: UserService) {}

  @Get('')
  async filter(@Query() filter: UserFilter): Response<IUser[]> {
    return this.userService.filter(filter);
  }

  @Get(':id')
  async getUserById(@Param('id', ParseIntPipe) id: number): Response<IUser> {
    const user = await this.userService.findByIdOrFail(id);
    return ResponseDTO.ok(user);
  }

  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: AdminUpdateUserDTO,
  ): Response {
    await this.userService.update(id, dto);
    return ResponseDTO.ok();
  }

  @Put(':id/password')
  async updatePassword(
    @Param('id', ParseIntPipe) id: number,
    @Body('password') newPassword: string,
  ): Response {
    await this.userService.updatePassword(id, newPassword);
    return ResponseDTO.ok();
  }

  @Post(':id/ban')
  async banUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() { reason }: BanUserDTO,
    @CurrentUser() user: IManager,
  ): Response {
    // TODO: Ban user
    return ResponseDTO.ok();
  }
}
