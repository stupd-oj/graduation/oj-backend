import { Body, Controller, Get, Param, Put } from '@nestjs/common';
import { UserService } from '../user.service';
import { Response, ResponseDTO } from '../../base/dto/response';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { UpdateUserDTO } from '../dto/update-user.dto';
import { UpdatePasswordDTO } from '../dto/update-password.dto';
import { Public } from '../../../common/decorators/metadata/public.decorator';
import { IUser } from '../interfaces/user.interface';
import { IProfile } from '../interfaces/profile.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { UpdateAvatarDto } from '../dto/update-avatar.dto';

@ApiTags(API_TAG.USER)
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('me')
  async getCurrentUser(@CurrentUser() user: IManager): Response<IUser> {
    const _user = await this.userService.findByIdOrFail(user.getIdentifier());
    return ResponseDTO.ok(_user);
  }

  @Put('me')
  async updateCurrentUser(
    @Body() dto: UpdateUserDTO,
    @CurrentUser() user: IManager,
  ): Response {
    await this.userService.update(user.getIdentifier(), dto);
    return ResponseDTO.ok();
  }

  @Put('me/password')
  async updateCurrentUserPassword(
    @Body() { oldPassword, newPassword }: UpdatePasswordDTO,
    @CurrentUser() user: IManager,
  ): Response {
    await this.userService.updatePassword(
      user.getIdentifier(),
      oldPassword,
      newPassword,
    );
    return ResponseDTO.ok();
  }

  @Put('me/avatar')
  async updateCurrentUserAvatar(
    @Body() { url }: UpdateAvatarDto,
    @CurrentUser() user: IManager,
  ): Response {
    await this.userService.update(user.getIdentifier(), { avatar: url });
    return ResponseDTO.ok();
  }

  @Public()
  @Get(':username')
  async getProfileByUserName(
    @Param('username') username: string,
  ): Response<IProfile> {
    return this.userService.getProfileByUsername(username);
  }
}
