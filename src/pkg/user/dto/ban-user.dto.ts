import { IsString } from 'class-validator';

export class BanUserDTO {
  @IsString()
  reason: string;
}
