import { BaseFilter } from '../../base/dto/filter';
import { IsOptional, IsString } from 'class-validator';
import { AcceptedSortField } from '../../../common/decorators/validations/accepted-sort-field.decorator';
import { PartialType } from '@nestjs/mapped-types';

const sortFields = [
  'u.id',
  'u.username',
  'u.fullName',
  'u.email',
  'u.isVerified',
  'u.createdAt',
  'u.phoneNumber',
  'u.role',
];

export class UserFilter extends PartialType(BaseFilter) {
  @IsOptional()
  @IsString()
  query: string;

  @IsOptional()
  @IsString()
  groupCode: string;

  @AcceptedSortField(...sortFields)
  sort: BaseFilter['sort'];
}
