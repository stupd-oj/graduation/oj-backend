import { IsString, Length } from 'class-validator';

export class UpdatePasswordDTO {
  @IsString()
  oldPassword: string;

  @IsString()
  @Length(6)
  newPassword: string;
}
