import {
  IsAlphanumeric,
  IsBoolean,
  IsDate,
  IsEmail,
  IsInt,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { IsVietnamese } from '../../../common/decorators/validations/is-vietnamese.decorator';
import { IUser } from '../interfaces/user.interface';

export class UpdateUserDTO implements Partial<IUser> {
  @IsString()
  phoneNumber: string;

  @IsString()
  @IsVietnamese()
  @Length(1, 30)
  fullName: string;

  @IsString()
  @Length(0, 255)
  mood: string;

  @IsOptional()
  @IsBoolean()
  gender: boolean;

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  birthday: Date;

  @IsString()
  @Length(0, 255)
  schoolName: string;

  @IsString()
  @Length(0, 30)
  className: string;

  @IsString()
  @Length(0, 30)
  studentId: string;
}

export class AdminUpdateUserDTO extends UpdateUserDTO {
  @IsInt()
  role: number;

  @Transform(({ value }) => value?.toLowerCase())
  @Length(3, 30)
  @IsAlphanumeric()
  username: string;

  @IsString()
  @Transform(({ value }) => value?.toLowerCase())
  @Length(0, 255)
  @IsEmail()
  email: string;
}
