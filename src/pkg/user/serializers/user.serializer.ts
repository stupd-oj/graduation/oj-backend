import { BaseModel } from '../../base/base.model';
import { IUser } from '../interfaces/user.interface';
import { UserRole } from '../constants/user-role';
import { IManager } from '../../../common/interfaces/role.interface';
import { Exclude, Expose } from 'class-transformer';
import { S3Config } from '../../../config/s3/s3.config';

export class User extends BaseModel implements IUser, IManager {
  @Exclude()
  avatar: string;
  birthday: Date;
  className: string;
  email: string;
  fullName: string;
  gender: boolean;
  id: number;
  isVerified: boolean;
  mood: string;
  phoneNumber: string;
  role: UserRole;
  schoolName: string;
  studentId: string;
  username: string;

  constructor(partial?: Partial<IUser>) {
    super();
    Object.assign(this, partial);
  }

  @Exclude()
  password: string;

  isAuthenticated(): boolean {
    return this.getIdentifier() !== -1;
  }

  hasRole(role: number): boolean {
    return this.role >= role;
  }

  getIdentifier(): number {
    return this.id;
  }

  @Expose({ name: 'avatar' })
  get getS3Avatar(): string {
    return `${S3Config.PUBLIC_ACCESS_POINT}/avatar/${this.id}`;
  }

  static withId(id: number): IUser {
    return new User({ id });
  }
}
