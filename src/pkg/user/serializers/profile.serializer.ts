import { BaseModel } from '../../base/base.model';
import { IProfile } from '../interfaces/profile.interface';
import { Number } from '../../../common/decorators/serializers/number.decorator';
import { User } from "./user.serializer";
import { Pick } from "../../../common/decorators/serializers/pick.decorator";

export class Profile extends BaseModel implements IProfile {
  avatar: string;

  fullName: string;

  gender: boolean;

  id: number;

  mood: string;

  @Number({ acceptNaN: true })
  totalContests: number;

  @Number({ acceptNaN: true })
  totalSolved: number;

  @Number({ acceptNaN: true })
  totalSubmissions: number;

  username: string;

  @Pick('schoolName', 'className', 'studentId')
  contact: User;

  constructor(profile: IProfile) {
    super();
    Object.assign(this, profile);
  }
}
