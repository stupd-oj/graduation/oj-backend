import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UserService, UserServiceImpl } from './user.service';
import { ManageUserController } from './controllers/manage-user.controller';
import { UserRepository } from './repositories/user.repository';
import { ProfileRepository } from './repositories/profile.repository';

@Module({
  imports: [],
  controllers: [UserController, ManageUserController],
  providers: [
    UserRepository,
    ProfileRepository,
    {
      provide: UserService,
      useClass: UserServiceImpl,
    },
  ],
  exports: [UserService],
})
export class UserModule {}
