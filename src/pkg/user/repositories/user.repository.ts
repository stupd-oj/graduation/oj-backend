import { DataSource } from 'typeorm';
import { UserEntity } from '../../../entities/sql/user.entity';
import { IUser, IUserRepository } from '../interfaces/user.interface';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { HttpException } from '../../../common/exceptions/http.exception';
import { User } from '../serializers/user.serializer';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const UserRepository: FactoryProvider<IUserRepository> = {
  provide: 'UserRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): IUserRepository => {
    const extended: Partial<IUserRepository> = {
      comparePassword(user: UserEntity, oldPassword: string): boolean {
        return user.comparePassword(oldPassword);
      },

      async updatePassword(user: UserEntity, password: string): Promise<void> {
        user.setPassword(password);
        await this.save(user);
      },

      findById(id: number): Promise<UserEntity> {
        return this.findOneBy({ id });
      },

      findByIdOrFail(id: number): Promise<UserEntity> {
        return this.findOneByOrFail({ id }).catch(() => {
          throw HttpException.notFound(PreDefinedError.USER_NOT_FOUND);
        });
      },

      findByIdentifier(identifier: string): Promise<UserEntity> {
        return this.findOneBy([
          { username: identifier },
          { email: identifier },
        ]);
      },

      findByIdentifierOrFail(identifier: string): Promise<IUser> {
        return this.findOneByOrFail([
          { username: identifier },
          { email: identifier },
        ]).catch(() => {
          throw HttpException.notFound(PreDefinedError.USER_NOT_FOUND);
        });
      },

      toSerializer(plain: IUser): User {
        return new User(plain);
      },

      toSerializerMany(plain: IUser[]): User[] {
        return plain.map(this.toSerializer);
      },
    };
    return datasource
      .getRepository(UserEntity)
      .extend(extended as IUserRepository);
  },
};
