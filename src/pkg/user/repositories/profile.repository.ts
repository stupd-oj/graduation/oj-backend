import { DataSource } from 'typeorm';
import { ProfileEntity } from '../../../entities/sql/views/profile.entity';
import { IProfile, IProfileRepository } from '../interfaces/profile.interface';
import { Profile } from '../serializers/profile.serializer';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const ProfileRepository: FactoryProvider<IProfileRepository> = {
  provide: 'ProfileRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): IProfileRepository => {
    const extended: Partial<IProfileRepository> = {
      toSerializer(plain: IProfile): Profile {
        return new Profile(plain);
      },

      toSerializerMany(plain: IProfile[]): Profile[] {
        return plain.map(this.toSerializer);
      },
    };
    return datasource
      .getRepository(ProfileEntity)
      .extend(extended as IProfileRepository);
  },
};
