import { IBaseRepository } from '../../base/base.repository';
import { Serializable } from '../../../common/interfaces/serializable.interface';
import { Profile } from '../serializers/profile.serializer';
import { IUser } from "./user.interface";

export interface IProfile {
  id: number;
  username: string;
  fullName: string;
  avatar: string;
  gender: boolean;
  mood: string;
  totalSubmissions: number;
  totalSolved: number;
  totalContests: number;
  contact?: IUser;
}

export interface IProfileRepository
  extends IBaseRepository<IProfile>,
    Serializable<IProfile, Profile> {}
