import { CanGetById, IBaseRepository } from '../../base/base.repository';
import { Serializable } from '../../../common/interfaces/serializable.interface';
import { User } from '../serializers/user.serializer';

export interface IUser {
  id: number;
  username: string;
  email: string;
  role: number;
  fullName: string;
  phoneNumber?: string;
  mood?: string;
  gender?: boolean;
  avatar?: string;
  birthday?: Date;
  schoolName?: string;
  className?: string;
  studentId?: string;
  isVerified: boolean;
}

export interface IUserRepository
  extends IBaseRepository<IUser>,
    Serializable<IUser, User>,
    CanGetById<IUser> {
  findByIdentifier(identifier: string): Promise<IUser>;

  findByIdentifierOrFail(identifier: string): Promise<IUser>;

  updatePassword(user: IUser, password: string): Promise<void>;

  comparePassword(user: IUser, oldPassword: string): boolean;
}
