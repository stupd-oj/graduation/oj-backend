import { DataSource, FindOneOptions, FindOptionsWhere } from 'typeorm';
import { ProblemEntity } from '../../../entities/sql/problem.entity';
import { IProblem, IProblemRepository } from '../interfaces/problem.interface';
import { Problem } from '../serializers/problem.serializer';
import { HttpException } from '../../../common/exceptions/http.exception';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const ProblemRepository: FactoryProvider<IProblemRepository> = {
  provide: 'ProblemRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): IProblemRepository => {
    const extended: Partial<IProblemRepository> = {
      findById(id, options?: FindOneOptions<IProblem>): Promise<IProblem> {
        return this.findOne({ ...options, where: { id } });
      },

      findByIdOrFail(
        id: number,
        options?: FindOneOptions<IProblem>,
      ): Promise<IProblem> {
        return this.findOneOrFail({ ...options, where: { id } }).catch(() => {
          throw HttpException.notFound(PreDefinedError.PROBLEM_NOT_FOUND);
        });
      },

      findOneByOrFail(
        where: FindOptionsWhere<IProblem> | FindOptionsWhere<IProblem>[],
      ): Promise<IProblem> {
        return this.findOneOrFail({ where }).catch(() => {
          throw HttpException.notFound(PreDefinedError.PROBLEM_NOT_FOUND);
        });
      },

      toSerializer(plain: IProblem): Problem {
        return new Problem(plain);
      },

      toSerializerMany(plain: IProblem[]): Problem[] {
        return plain.map(this.toSerializer);
      },
    };
    return datasource
      .getRepository(ProblemEntity)
      .extend(extended as IProblemRepository);
  },
};
