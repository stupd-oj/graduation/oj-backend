import { CanGetById, IBaseRepository } from '../../base/base.repository';
import { IContest } from '../../contest/interfaces/contest.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { Serializable } from '../../../common/interfaces/serializable.interface';
import { Problem } from '../serializers/problem.serializer';
import { IProblemTest } from './problem-test.interface';

export interface IProblem {
  id: number;
  code: string;
  name: string;
  description: string;
  hint: string;
  startAt: Date;
  endAt: Date;
  timeLimit: number;
  memoryLimit: number;
  active: boolean;
  level: number;
  firstSolved?: number;
  multiplier: number;
  contest?: IContest;
  contestId: IContest['id'];
  createdBy: IUser;
  createdById: IUser['id'];
  tests?: IProblemTest[];

  canSubmit?: boolean;
}

export interface IProblemRepository
  extends IBaseRepository<IProblem>,
    Serializable<IProblem, Problem>,
    CanGetById<IProblem> {}
