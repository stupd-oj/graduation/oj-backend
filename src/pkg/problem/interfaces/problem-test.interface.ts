import { IProblem } from './problem.interface';

export interface IProblemTest {
  uuid: string;
  number: number;
  inputUri: string;
  outputUri: string;
  active: boolean;
  problem?: IProblem;
  problemId: IProblem['id'];
}
