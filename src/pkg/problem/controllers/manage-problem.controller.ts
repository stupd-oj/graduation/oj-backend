import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  SerializeOptions,
} from '@nestjs/common';
import { ProblemService } from '../problem.service';
import { Response, ResponseDTO } from '../../base/dto/response';
import { CreateProblemDTO, UpdateProblemDTO } from '../dto/problem.dto';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { Role } from '../../../common/decorators/metadata/role.decorator';
import { UserRole } from '../../user/constants/user-role';
import { ProblemFilter } from '../dto/problem-filter.query';
import { ContestService } from '../../contest/contest.service';
import { IProblem } from '../interfaces/problem.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { User } from '../../user/serializers/user.serializer';
import { SerialGroup } from '../../../common/constants/serial-group';

@ApiTags(API_TAG.MANAGE_PROBLEM)
@Role(UserRole.Admin)
@Controller('admin/problems')
@SerializeOptions({ groups: [SerialGroup.Admin] })
export class ManageProblemController {
  constructor(
    private readonly problemService: ProblemService,
    private readonly contestService: ContestService,
  ) {}

  @Get('')
  async filter(
    @Query() filter: ProblemFilter,
    @CurrentUser() user: IManager,
  ): Response<IProblem[]> {
    const contest = await this.contestService.findByIdOrFail(filter.contestId);
    contest.checkReadAuthority(user);

    return this.problemService.filter(filter, true);
  }

  @Get('all')
  async getAll(): Response<IProblem[]> {
    return ResponseDTO.ok(await this.problemService.findAll());
  }

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) problemId: number,
    @CurrentUser() user: IManager,
  ): Response<IProblem> {
    const problem = await this.problemService.findByIdOrFail(problemId, true);
    problem.checkReadAuthority(user);

    return ResponseDTO.ok(problem);
  }

  @Post('')
  async create(
    @Body() dto: CreateProblemDTO,
    @CurrentUser() user: IManager,
  ): Response<number> {
    const contest = await this.contestService.findByIdOrFail(dto.contestId);
    contest.checkWriteAuthority(user);

    const problem = await this.problemService.create({
      ...dto,
      contest,
      createdBy: User.withId(user.getIdentifier()),
    });
    return ResponseDTO.ok(problem.id);
  }

  @Put(':id')
  async update(
    @Body() dto: UpdateProblemDTO,
    @Param('id', ParseIntPipe) problemId: number,
    @CurrentUser() user: IManager,
  ): Response {
    const problem = await this.problemService.findByIdOrFail(problemId);
    problem.checkWriteAuthority(user);

    await this.problemService.update(problemId, dto);
    return ResponseDTO.ok();
  }

  @Delete(':id')
  async delete(
    @Param('id', ParseIntPipe) problemId: number,
    @CurrentUser() user: IManager,
  ): Response {
    const problem = await this.problemService.findByIdOrFail(problemId);
    problem.checkWriteAuthority(user);

    await this.problemService.remove(problemId);
    return ResponseDTO.ok();
  }
}
