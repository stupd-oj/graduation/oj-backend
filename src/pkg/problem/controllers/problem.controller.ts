import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ProblemService } from '../problem.service';
import { Response, ResponseDTO } from '../../base/dto/response';
import { Public } from '../../../common/decorators/metadata/public.decorator';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { HttpException } from '../../../common/exceptions/http.exception';
import { ProblemFilter } from '../dto/problem-filter.query';
import { ContestService } from '../../contest/contest.service';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { IProblem } from '../interfaces/problem.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';

@ApiTags(API_TAG.PROBLEM)
@Controller('problems')
export class ProblemController {
  constructor(
    private readonly problemService: ProblemService,
    private readonly contestService: ContestService,
  ) {}

  @Get('')
  async filter(
    @Query() filter: ProblemFilter,
    @CurrentUser() user: IManager,
  ): Response<IProblem[]> {
    return this.problemService.filter(filter, false);
  }

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) problemId: number,
    @CurrentUser() user: IManager,
  ): Response<IProblem> {
    const problem = await this.problemService.findByIdOrFail(problemId);
    if (!problem?.active)
      throw HttpException.notFound(PreDefinedError.PROBLEM_NOT_FOUND);

    const contest = await this.contestService.findByIdOrFail(problem.contestId);
    const isContestant = await this.contestService.isContestant(
      problem.contestId,
      user.getIdentifier(),
    );
    if (contest.isPrivate && !isContestant)
      throw HttpException.forbidden(PreDefinedError.CONTEST_IS_PRIVATE);
    if (contest.startAt > new Date()) {
      throw HttpException.forbidden(PreDefinedError.PROBLEM_NOT_START);
    }

    problem.contest = contest;
    problem.canSubmit = await this.problemService.userCanSubmit(
      problem,
      user.getIdentifier(),
    );

    return ResponseDTO.ok(problem);
  }
}
