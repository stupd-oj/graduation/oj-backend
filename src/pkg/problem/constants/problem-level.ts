export enum ProblemLevel {
  EASY = 0,
  MEDIUM = 1,
  HARD = 2,
}
