import { Module } from '@nestjs/common';
import { ProblemService, ProblemServiceImpl } from './problem.service';
import { ProblemController } from './controllers/problem.controller';
import { ContestModule } from '../contest/contest.module';
import { ProblemRepository } from './repositories/problem.repository';
import { ManageProblemController } from './controllers/manage-problem.controller';

@Module({
  imports: [ContestModule],
  providers: [
    ProblemRepository,
    {
      provide: ProblemService,
      useClass: ProblemServiceImpl,
    },
  ],
  controllers: [ProblemController, ManageProblemController],
  exports: [ProblemService],
})
export class ProblemModule {}
