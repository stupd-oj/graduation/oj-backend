import { IProblemTest } from '../interfaces/problem-test.interface';
import { BaseModel } from '../../base/base.model';
import { IProblem } from '../interfaces/problem.interface';
import { Exclude } from 'class-transformer';

export class ProblemTest extends BaseModel implements IProblemTest {
  active: boolean;
  number: number;
  inputUri: string;
  outputUri: string;
  problemId: IProblem['id'];
  uuid: string;

  @Exclude()
  problem?: IProblem;
}
