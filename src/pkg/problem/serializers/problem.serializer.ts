import { BaseModel } from '../../base/base.model';
import { IProblem } from '../interfaces/problem.interface';
import { Contest } from '../../contest/serializers/contest.serializer';
import { User } from '../../user/serializers/user.serializer';
import {
  AuthorityException,
  AuthorityType,
  IManageable,
} from '../../../common/interfaces/manageable.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { anyTruthy } from '../../../common/helpers/array.helper';
import { UserRole } from '../../user/constants/user-role';
import { ProblemTest } from './problem-test.serializer';
import { Expose, Type } from 'class-transformer';
import { IProblemTest } from '../interfaces/problem-test.interface';
import { IContest } from '../../contest/interfaces/contest.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { Pick } from '../../../common/decorators/serializers/pick.decorator';
import { SerialGroup } from '../../../common/constants/serial-group';

export class Problem extends BaseModel implements IProblem, IManageable {
  active: boolean;
  code: string;
  description: string;
  endAt: Date;
  firstSolved: number;
  hint: string;
  id: number;
  level: number;
  memoryLimit: number;
  multiplier: number;
  name: string;
  startAt: Date;
  timeLimit: number;
  canSubmit?: boolean;

  @Type(() => Contest)
  contest?: IContest;

  contestId: IContest['id'];

  @Type(() => User)
  @Pick('id', 'username', 'avatar', 'fullName')
  createdBy: IUser;

  createdById: IUser['id'];

  @Type(() => ProblemTest)
  @Expose({ groups: [SerialGroup.Admin] })
  tests?: IProblemTest[];

  constructor(partial?: Partial<IProblem>) {
    super();
    Object.assign(this, partial);
  }

  checkReadAuthority(manager: IManager): void {
    if (
      !anyTruthy(
        manager.hasRole(UserRole.Admin),
        this.createdById === manager.getIdentifier(),
      )
    )
      throw AuthorityException.of(Problem.name, AuthorityType.read);
  }

  checkWriteAuthority(manager: IManager): void {
    if (
      !anyTruthy(
        manager.hasRole(UserRole.Admin),
        this.createdById === manager.getIdentifier(),
      )
    )
      throw AuthorityException.of(Problem.name, AuthorityType.write);
  }

  static withId(id: number): IProblem {
    return new Problem({ id });
  }
}
