import { Inject, Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { ProblemFilter } from './dto/problem-filter.query';
import { ProblemRepository } from './repositories/problem.repository';
import { Response, ResponseDTO } from '../base/dto/response';
import { DataSource } from 'typeorm';
import { ContestService } from '../contest/contest.service';
import { Problem } from './serializers/problem.serializer';
import { IProblem, IProblemRepository } from './interfaces/problem.interface';
import { isBetween } from '../../common/helpers/common.helper';
import { allTruthy } from '../../common/helpers/array.helper';
import { omit, pick } from 'lodash';

export abstract class ProblemService {
  abstract findAll(): Promise<Problem[]>;

  abstract filter(
    filter: ProblemFilter,
    isManager: boolean,
  ): Response<Problem[]>;

  abstract findByIdOrFail(id: number, loadTests?: boolean): Promise<Problem>;

  abstract userCanSubmit(problem: IProblem, userId: number): Promise<boolean>;

  abstract create(partial: Partial<IProblem>): Promise<Problem>;

  abstract update(id: number, partial: Partial<IProblem>): Promise<void>;

  abstract remove(id: number): Promise<void>;
}

@Injectable()
export class ProblemServiceImpl extends ProblemService {
  constructor(
    @Inject(ProblemRepository.provide)
    private readonly problemRepository: IProblemRepository,
    @InjectDataSource() private readonly dataSource: DataSource,
    private readonly contestService: ContestService,
  ) {
    super();
  }

  async filter(filter: ProblemFilter, isManager: boolean): Response<Problem[]> {
    const {
      offset,
      limit,
      sort = [],
      query,
      level,
      contestId,
      fields,
    } = filter;
    const queryBuilder = this.problemRepository.createQueryBuilder('p');

    queryBuilder.leftJoinAndMapOne('p.createdBy', 'p.createdBy', 'u');

    queryBuilder.where(`p.contest_id = :contestId`, {
      contestId,
    });

    if (query) {
      queryBuilder.andWhere(`(p.name ILIKE :text OR p.code ILIKE :text)`, {
        text: `%${query}%`,
      });
    }

    if (level) {
      queryBuilder.andWhere(`p.level = :level`, { level });
    }

    if (!isManager) {
      queryBuilder.andWhere(`p.active`);
      queryBuilder.andWhere(`p.startAt < NOW()`);
    }

    for (const s of sort) {
      queryBuilder.orderBy(s.field, s.direction);
    }

    queryBuilder.take(limit);
    queryBuilder.skip(offset);
    let [entities, total] = await queryBuilder.getManyAndCount();
    await this.getStatusCount(...entities);
    entities = entities.map(
      (e) => omit(e, ['description', 'hint']) as IProblem,
    );

    if (fields) {
      entities = entities.map((e) => pick(e, fields.split(',')) as IProblem);
    }
    return ResponseDTO.ok(
      this.problemRepository.toSerializerMany(entities),
      total,
    );
  }

  async getStatusCount(...problems: IProblem[]): Promise<void> {
    // TODO: HANDLE LATER
  }

  async findByIdOrFail(id: number, loadTests?: boolean): Promise<Problem> {
    const relations = loadTests ? ['tests'] : [];
    return this.problemRepository
      .findByIdOrFail(id, { relations })
      .then(this.problemRepository.toSerializer);
  }

  async userCanSubmit(problem: IProblem, userId: number): Promise<boolean> {
    const isContestant = await this.contestService.isContestant(
      problem.contestId,
      userId,
    );
    const contest = await this.contestService.findByIdOrFail(problem.contestId);
    const now = new Date();
    return allTruthy(
      isContestant,
      problem.active,
      isBetween(now, contest.startAt, contest.endAt),
    );
  }

  async create(partial: Partial<IProblem>): Promise<Problem> {
    const problem = this.problemRepository.create(partial);
    if (!problem.startAt) problem.startAt = problem.contest.startAt;
    if (!problem.endAt) problem.endAt = problem.contest.endAt;
    await this.problemRepository.save(problem);
    return this.problemRepository.toSerializer(problem);
  }

  async update(id: number, partial: Partial<IProblem>): Promise<void> {
    const problem = await this.problemRepository.findByIdOrFail(id);
    this.problemRepository.merge(problem, partial);
    await this.problemRepository.save(problem);
  }

  async remove(id: number): Promise<void> {
    const problem = await this.problemRepository.findByIdOrFail(id);
    await this.problemRepository.remove(problem);
  }

  async findAll(): Promise<Problem[]> {
    const plain = await this.problemRepository.find({ relations: ['contest'] });
    return this.problemRepository.toSerializerMany(plain);
  }
}
