import { IProblemTest } from '../interfaces/problem-test.interface';
import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsUrl,
  IsUUID,
} from 'class-validator';

export class ProblemTestDTO implements Partial<IProblemTest> {
  @IsOptional()
  @IsUUID()
  uuid: string;

  @IsNumber()
  number: number;

  @IsUrl()
  inputUri: string;

  @IsUrl()
  outputUri: string;

  @IsBoolean()
  active: boolean;
}
