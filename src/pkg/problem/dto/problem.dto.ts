import {
  ArrayMaxSize,
  IsAlphanumeric,
  IsBoolean,
  IsDateString,
  IsIn,
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  Max,
  MaxLength,
  Min,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ProblemLevel } from '../constants/problem-level';
import { IProblem } from '../interfaces/problem.interface';
import { ProblemConstant } from '../constants/problem-constant';
import { ProblemTestDTO } from './problem-test.dto';
import { IProblemTest } from '../interfaces/problem-test.interface';

export class UpdateProblemDTO implements Partial<IProblem> {
  @IsString()
  @Length(ProblemConstant.MIN_LENGTH_NAME, ProblemConstant.MAX_LENGTH_NAME)
  name: string;

  @IsString()
  @MaxLength(ProblemConstant.MAX_LENGTH_DESCRIPTION)
  description: string;

  @IsOptional()
  @IsString()
  @MaxLength(ProblemConstant.MAX_LENGTH_HINT)
  hint: string;

  @IsOptional()
  @IsDateString()
  startAt: Date;

  @IsOptional()
  @IsDateString()
  endAt: Date;

  @IsString()
  @IsAlphanumeric()
  @MaxLength(ProblemConstant.MAX_LENGTH_CODE)
  @Transform(({ value }) => value.toUpperCase())
  code: string;

  @IsNumber()
  @Min(ProblemConstant.MIN_TIME_LIMIT)
  @Max(ProblemConstant.MAX_TIME_LIMIT)
  timeLimit: number;

  @IsNumber()
  @Min(ProblemConstant.MIN_MEMORY_LIMIT)
  @Max(ProblemConstant.MAX_MEMORY_LIMIT)
  memoryLimit: number;

  @IsBoolean()
  active: boolean;

  @IsInt()
  @IsIn([ProblemLevel.EASY, ProblemLevel.MEDIUM, ProblemLevel.HARD])
  level: ProblemLevel;

  @Type(() => ProblemTestDTO)
  @ValidateNested({ each: true })
  @ArrayMaxSize(ProblemConstant.MAX_TESTS)
  tests: IProblemTest[];
}

export class CreateProblemDTO extends UpdateProblemDTO {
  @IsInt()
  contestId: number;

  @IsInt()
  @Min(ProblemConstant.MIN_MULTIPLIER)
  @Max(ProblemConstant.MAX_MULTIPLIER)
  multiplier: number;
}
