import { BaseFilter } from '../../base/dto/filter';
import { IsIn, IsInt, IsOptional, IsString } from 'class-validator';
import { ProblemLevel } from '../constants/problem-level';
import { Number } from '../../../common/decorators/serializers/number.decorator';
import { PartialType } from '@nestjs/mapped-types';
import { Sort } from '../../base/dto/sort';
import { AcceptedSortField } from '../../../common/decorators/validations/accepted-sort-field.decorator';

const sortFields = ['p.id', 'p.code', 'p.name', 'p.level'];

export class ProblemFilter extends PartialType(BaseFilter) {
  @Number()
  @IsInt()
  contestId: number;

  @IsOptional()
  @IsString()
  query: string;

  @IsOptional()
  @IsString()
  @IsIn([ProblemLevel.EASY, ProblemLevel.MEDIUM, ProblemLevel.HARD])
  level: ProblemLevel;

  @AcceptedSortField(...sortFields)
  sort: Array<Sort>;

  @IsOptional()
  fields: string;
}
