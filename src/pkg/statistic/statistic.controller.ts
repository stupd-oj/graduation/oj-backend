import { Controller, Get } from '@nestjs/common';
import { ResponseDTO } from '../base/dto/response';
import { Role } from '../../common/decorators/metadata/role.decorator';
import { UserRole } from '../user/constants/user-role';
import { StatisticService } from './statistic.service';

@Controller('statistic')
@Role(UserRole.Admin)
export class StatisticController {
  constructor(private readonly statisticService: StatisticService) {}

  @Get('contest-timeline')
  async getContestTimeline() {
    return ResponseDTO.ok(await this.statisticService.getContestTimeline());
  }

  @Get('system-summary')
  async getSystemSummary() {
    return ResponseDTO.ok(await this.statisticService.getSystemSummary());
  }

  @Get('submission-report-by-date')
  async getSubmissionReportByDate() {
    return ResponseDTO.ok(
      await this.statisticService.getSubmissionReportByDate(),
    );
  }

  @Get('submission-report-by-status')
  async getSubmissionReportByStatus() {
    return ResponseDTO.ok(
      await this.statisticService.getSubmissionReportByStatus(),
    );
  }
}
