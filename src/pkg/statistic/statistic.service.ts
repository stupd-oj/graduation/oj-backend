import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';

@Injectable()
export class StatisticService {
  constructor(private readonly entityManager: EntityManager) {}

  async getContestTimeline() {
    return this.entityManager
      .query(`select c.id, c.name, c.start_at, c.end_at, c.rule
                                     from contest c`);
  }

  async getSystemSummary() {
    return {
      contest: await this.entityManager
        .query(
          `select count(1)
           from "contest";`,
        )
        .then((res) => res[0]),
      user: await this.entityManager
        .query(
          `select count(1)
           from "user";`,
        )
        .then((res) => res[0]),
      submission: await this.entityManager
        .query(
          `select count(1)
           from "submission";`,
        )
        .then((res) => res[0]),
    };
  }

  async getSubmissionReportByDate() {
    return this.entityManager
      .query(`with submission_report as (select s.*, date (s.created_at) as date
              from submission s)
      select count(1), sr.date
      from submission_report sr
      group by sr.date
      order by sr.date;`);
  }

  async getSubmissionReportByStatus() {
    return this.entityManager.query(`select s.status, count(1)
                                     from submission s
                                     group by s.status;`);
  }
}
