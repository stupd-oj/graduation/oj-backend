import { BaseModel } from '../../base/base.model';
import { IProblemHighScore } from '../interfaces/problem-high-score.interface';
import { Problem } from '../../problem/serializers/problem.serializer';
import { Type } from 'class-transformer';
import { IUser } from '../../user/interfaces/user.interface';
import { IProblem } from '../../problem/interfaces/problem.interface';
import { ISubmission } from '../../submission/interfaces/submission.interface';
import { User } from '../../user/serializers/user.serializer';

export class ProblemHighScore extends BaseModel implements IProblemHighScore {
  @Type(() => Problem)
  problem: IProblem;
  problemId: IProblem['id'];
  score: number;
  solved: boolean;
  submissionRefId: ISubmission['id'];
  time: number;
  tried: number;
  user: IUser;
  @Type(() => User)
  userId: IUser['id'];
}
