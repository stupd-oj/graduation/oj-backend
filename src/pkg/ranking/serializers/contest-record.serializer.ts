import { IContestScore } from '../interfaces/contest-score.interface';
import { IProblemHighScore } from '../interfaces/problem-high-score.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { Type } from 'class-transformer';
import { User } from '../../user/serializers/user.serializer';
import { Pick } from '../../../common/decorators/serializers/pick.decorator';

export class ContestScore implements IContestScore {
  contestId: number;
  rank: number;
  @Type(() => User)
  @Pick('id', 'username', 'avatar', 'fullName')
  user: IUser;
  userId: number;
  records: Array<IProblemHighScore>;
}
