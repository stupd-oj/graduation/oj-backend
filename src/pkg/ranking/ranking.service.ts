import { Injectable, Logger } from '@nestjs/common';
import { RankingFilterDTO } from './dto/ranking-filter.dto';
import { Response, ResponseDTO } from '../base/dto/response';
import { ProblemService } from '../problem/problem.service';
import { Submission } from '../submission/serializers/submission.serializer';
import { ISubmission } from '../submission/interfaces/submission.interface';
import { ContestService } from '../contest/contest.service';
import dateHelper from '../../common/helpers/date.helper';
import { allTruthy } from '../../common/helpers/array.helper';
import { SubmissionStatus } from '../submission/constants/submission-status';
import { isNil } from 'lodash';
import { EntityManager, Repository } from 'typeorm';
import { IProblemHighScore } from './interfaces/problem-high-score.interface';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { ProblemHighScoreEntity } from '../../entities/sql/problem-high-score.entity';
import { IContestScore } from './interfaces/contest-score.interface';

export abstract class RankingService {
  abstract updateProblemHighScore(submission: ISubmission): Promise<void>;

  abstract getLeaderboard(
    contestId: number,
    filter: RankingFilterDTO,
    realTime?: boolean,
  ): Response<IContestScore[]>;

  abstract updatePersistentLeaderboard(contestId?: number): Promise<void>;
}

@Injectable()
export class RankingServiceImpl extends RankingService {
  private readonly logger = new Logger(RankingService.name);

  constructor(
    @InjectRepository(ProblemHighScoreEntity)
    private readonly problemHighScoreRepository: Repository<IProblemHighScore>,
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
    private readonly problemService: ProblemService,
    private readonly contestService: ContestService,
  ) {
    super();
  }

  async updateProblemHighScore(submission: Submission): Promise<void> {
    try {
      const problemId = submission.problemId ?? submission.problem.id;
      const problem = await this.problemService.findByIdOrFail(problemId);

      const submissionTimeFromStart = Math.ceil(
        dateHelper.diff(problem.startAt, submission.createdAt, 'minutes'),
      );

      if (
        allTruthy(
          submission.status === SubmissionStatus.AC,
          isNil(problem.firstSolved) ||
            submissionTimeFromStart < problem.firstSolved,
        )
      ) {
        await this.problemService.update(problem.id, {
          firstSolved: submissionTimeFromStart,
        });
      }

      const problemHighScore = await this.problemHighScoreRepository.findOneBy({
        problemId,
        userId: submission.createdById,
      });

      if (problemHighScore) {
        if (!problemHighScore.solved) {
          problemHighScore.tried++;
          problemHighScore.solved = submission.status === SubmissionStatus.AC;

          if (submission.score > problemHighScore.score) {
            problemHighScore.score = submission.score;
            problemHighScore.time = submissionTimeFromStart;
            problemHighScore.submissionRefId = submission.id;
          }
        }

        // Case re-judge
        if (
          allTruthy(
            submission.score === problemHighScore.score,
            submissionTimeFromStart < problemHighScore.time,
          )
        ) {
          problemHighScore.time = submissionTimeFromStart;
          problemHighScore.submissionRefId = submission.id;
        }
        await this.problemHighScoreRepository.save(problemHighScore);
      } else {
        await this.createProblemHighScore({
          problemId,
          user: submission.createdBy,
          score: submission.score ?? 0,
          time: submissionTimeFromStart,
          solved: submission.status === SubmissionStatus.AC,
          submissionRefId: submission.id,
        });
      }
    } catch (e) {
      this.logger.error(`Rank updated error`);
      this.logger.error(e);
    }
  }

  async createProblemHighScore(
    partial: Partial<IProblemHighScore>,
  ): Promise<void> {
    const problemHighScore = this.problemHighScoreRepository.create(partial);
    await this.problemHighScoreRepository.save(problemHighScore);
  }

  async updatePersistentLeaderboard(contestId?: number): Promise<void> {
    const contestIds = contestId
      ? [contestId]
      : await this.contestService.findRankUpdatableIds();
    for (const contestId of contestIds) {
      await this.entityManager
        //language=SQL
        .query('CALL update_contest_rank($1)', [contestId])
        .catch(this.logger.error);
    }
    return;
  }

  async getLeaderboard(
    contestId: number,
    filter: RankingFilterDTO,
    realTime: boolean = true,
  ): Response<IContestScore[]> {
    const { limit, offset } = filter;
    //language=SQL
    const result: IContestScore[] = await this.entityManager.query(
      `
          SELECT c.user_id                                        AS user_id,
                 COALESCE(r.score, 0)                             AS score,
                 COALESCE(r.time, 0)                              AS time,
                 r.rank                                           AS rank,
                 COALESCE(r.records, '[]')                        AS records,
                 jsonb_build_object('full_name', u."full_name", 'avatar', u."avatar", 'username',
                                    u."username", 'mood', u.mood) AS "user"
          FROM contestant c
                   LEFT JOIN get_contest_rank($1, $2) r ON r.user_id = c.user_id
                   LEFT JOIN "user" u ON c."user_id" = u.id
          WHERE c.contest_id = $1
          ORDER BY r.rank
          LIMIT $3 OFFSET $4;
      `,
      [contestId, realTime, limit, offset],
    );

    const count: number = (
      await this.entityManager.query(
        `
            SELECT COUNT(c.*)::INT AS "count"
            FROM contestant c
            WHERE c.contest_id = $1
        `,
        [contestId],
      )
    )[0].count;
    return ResponseDTO.ok(result, count);
  }
}
