import { Module } from '@nestjs/common';
import { RankingService, RankingServiceImpl } from './ranking.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RankingController } from './ranking.controller';
import { ContestModule } from '../contest/contest.module';
import { ProblemModule } from '../problem/problem.module';
import { ProblemHighScoreEntity } from '../../entities/sql/problem-high-score.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProblemHighScoreEntity]),
    ContestModule,
    ProblemModule,
  ],
  providers: [
    {
      provide: RankingService,
      useClass: RankingServiceImpl,
    },
  ],
  exports: [RankingService],
  controllers: [RankingController],
})
export class RankingModule {}
