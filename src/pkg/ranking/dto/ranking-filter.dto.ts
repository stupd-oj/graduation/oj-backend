import { BaseFilter } from '../../base/dto/filter';
import { IsOptional, IsString } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';

export class RankingFilterDTO extends PartialType(BaseFilter) {
  @IsOptional()
  @IsString()
  query: string;
}
