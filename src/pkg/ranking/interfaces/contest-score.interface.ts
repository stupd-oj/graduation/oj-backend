import { IProblemHighScore } from './problem-high-score.interface';
import { IUser } from '../../user/interfaces/user.interface';

export interface IContestScore {
  rank?: number;
  contestId?: number;
  userId?: number;
  user?: IUser;
  records?: Array<IProblemHighScore>;
}
