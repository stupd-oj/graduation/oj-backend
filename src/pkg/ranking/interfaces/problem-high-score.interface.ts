import { ISubmission } from '../../submission/interfaces/submission.interface';
import { IProblem } from '../../problem/interfaces/problem.interface';
import { IUser } from '../../user/interfaces/user.interface';

export interface IProblemHighScore {
  score: number;
  tried: number;
  time: number;
  solved: boolean;
  submissionRefId?: ISubmission['id'];
  problem?: IProblem;
  user?: IUser;
  problemId: IProblem['id'];
  userId: IUser['id'];
}
