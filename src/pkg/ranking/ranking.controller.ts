import {
  CacheInterceptor,
  CacheTTL,
  Controller,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { RankingService } from './ranking.service';
import { RankingFilterDTO } from './dto/ranking-filter.dto';
import { OnEvent } from '@nestjs/event-emitter';
import { Event as InternalEvent } from '../../providers/event/constants/event';
import { ISubmission } from '../submission/interfaces/submission.interface';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { ScheduleJob } from '../../providers/schedule/constants/job';
import { ContestService } from '../contest/contest.service';
import { Role } from '../../common/decorators/metadata/role.decorator';
import { UserRole } from '../user/constants/user-role';
import { CurrentUser } from '../../common/decorators/requests/current-user.decorator';
import { IManager } from '../../common/interfaces/role.interface';
import { ResponseDTO } from '../base/dto/response';

@Controller('leaderboard')
export class RankingController {
  private readonly logger = new Logger(RankingController.name);

  constructor(
    private readonly rankingService: RankingService,
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly contestService: ContestService,
  ) {}

  @UseInterceptors(CacheInterceptor)
  @CacheTTL(10) // seconds
  @Get(':contestId')
  async getContestLeaderBoard(
    @Query() query: RankingFilterDTO,
    @Param('contestId', ParseIntPipe) contestId: number,
  ) {
    const contest = await this.contestService.findByIdOrFail(contestId);
    return this.rankingService.getLeaderboard(
      contestId,
      query,
      contest.updatedRankAt < contest.closeRankAt,
    );
  }

  @Role(UserRole.Admin)
  @Get(':contestId/real')
  async getContestLeaderBoardRealtime(
    @Query() query: RankingFilterDTO,
    @Param('contestId', ParseIntPipe) contestId: number,
    @CurrentUser() user: IManager,
  ) {
    const contest = await this.contestService.findByIdOrFail(contestId);
    contest.checkReadAuthority(user);
    return this.rankingService.getLeaderboard(contestId, query, true);
  }

  @Role(UserRole.Admin)
  @Post(':contestId/update')
  async updatePersistentLeaderboard(
    @Param('contestId', ParseIntPipe) contestId: number,
    @CurrentUser() user: IManager,
  ) {
    const contest = await this.contestService.findByIdOrFail(contestId);
    contest.checkWriteAuthority(user);
    await this.rankingService.updatePersistentLeaderboard(contestId);
    return ResponseDTO.ok();
  }

  @OnEvent(InternalEvent.SUBMISSION_RESULT_UPDATED)
  onSubmissionUpdated(submission: ISubmission): void {
    this.rankingService.updateProblemHighScore(submission).catch();
  }

  @Cron(CronExpression.EVERY_30_MINUTES, {
    name: ScheduleJob.UPDATE_RANK,
  })
  async handleUpdateRank() {
    this.logger.log('Start update persistent rank');
    const job = this.schedulerRegistry.getCronJob(ScheduleJob.UPDATE_RANK);
    job.stop();
    await this.rankingService.updatePersistentLeaderboard().catch();
    job.start();
    this.logger.log('End update persistent rank');
  }
}
