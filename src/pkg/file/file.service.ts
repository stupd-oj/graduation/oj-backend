import { Injectable } from '@nestjs/common';
import { S3Config } from '../../config/s3/s3.config';
import {
  PutObjectCommand,
  PutObjectCommandInput,
  S3Client,
} from '@aws-sdk/client-s3';

import { IManager } from '../../common/interfaces/role.interface';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { UserRole } from '../user/constants/user-role';
import { HttpException } from '../../common/exceptions/http.exception';
import { replace } from 'lodash';

export abstract class FileService {
  public static AVATAR_PREFIX = 'avatar';
  public static IMAGE_PREFIX = 'images';
  public static PROBLEM_TEST_PREFIX = 'testcases';
  abstract getPreSignedUploadUrl(
    objectKey: string,
    user: IManager,
  ): Promise<string>;
  abstract getPublicUrl(objectKey: string): string;
}

@Injectable()
export class FileServiceImpl extends FileService {
  private readonly DEFAULT_EXPIRE_TIME = 600;
  private readonly s3Client: S3Client;
  constructor(private readonly s3Config: S3Config) {
    super();
    this.s3Client = new S3Client({
      endpoint: this.s3Config.endPoint,
      credentials: {
        accessKeyId: this.s3Config.accessKeyId,
        secretAccessKey: this.s3Config.secretAccessKey,
      },
      region: this.s3Config.region,
    });
  }

  async getPreSignedUploadUrl(
    objectKey: string,
    user: IManager,
  ): Promise<string> {
    objectKey = replace(objectKey, this.s3Config.publicEndpoint, '');
    if (!this.checkPermission(user, objectKey)) {
      throw HttpException.forbidden();
    }
    const putObjectCommand = this.getPutObjectCommand(objectKey);
    return getSignedUrl(this.s3Client, putObjectCommand, {
      expiresIn: this.DEFAULT_EXPIRE_TIME,
    });
  }

  private getPutObjectCommand(objectKey: string): PutObjectCommand {
    if (!objectKey) {
      throw new Error('Object key is required');
    }
    const commandOptions: Partial<PutObjectCommandInput> = {};

    if (objectKey.startsWith(FileService.AVATAR_PREFIX)) {
      commandOptions.ContentType = 'image/*';
    }

    return new PutObjectCommand({
      Key: objectKey,
      Bucket: this.s3Config.bucketName,
      ...commandOptions,
    });
  }

  private checkPermission(user: IManager, objectKey: string): boolean {
    if (!objectKey) return false;
    if (objectKey.startsWith(FileServiceImpl.AVATAR_PREFIX)) {
      return (
        objectKey === `${FileServiceImpl.AVATAR_PREFIX}/${user.getIdentifier()}`
      );
    }

    return user.hasRole(UserRole.Admin);
  }

  getPublicUrl(objectKey: string): string {
    return `${this.s3Config.publicEndpoint}/${objectKey}`;
  }
}
