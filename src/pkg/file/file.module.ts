import { Module } from '@nestjs/common';
import { S3ConfigModule } from '../../config/s3/s3-config.module';
import { FileController } from './file.controller';
import { FileService, FileServiceImpl } from './file.service';

@Module({
  imports: [S3ConfigModule],
  controllers: [FileController],
  providers: [
    {
      provide: FileService,
      useClass: FileServiceImpl,
    },
  ],
  exports: [],
})
export class FileModule {}
