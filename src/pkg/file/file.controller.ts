import { Body, Controller, Post } from '@nestjs/common';
import { FileService } from './file.service';
import { Response, ResponseDTO } from '../base/dto/response';
import { CurrentUser } from '../../common/decorators/requests/current-user.decorator';
import { IManager } from '../../common/interfaces/role.interface';

@Controller('files')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post('upload-url')
  async getPreSignedUploadUrl(
    @Body('objectKey') objectKey: string,
    @CurrentUser() user: IManager,
  ): Response<any> {
    const url = await this.fileService.getPreSignedUploadUrl(objectKey, user);
    return ResponseDTO.ok({
      preSignedUrl: url,
      publicUrl: this.fileService.getPublicUrl(objectKey),
    });
  }
}
