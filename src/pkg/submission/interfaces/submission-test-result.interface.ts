import { ISubmission } from './submission.interface';

export interface ISubmissionTestResult {
  result: string;
  memory?: number;
  time?: number;
  testUuid: string;
  submission?: ISubmission;
  submissionId: ISubmission['id'];
}
