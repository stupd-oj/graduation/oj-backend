import { CanGetById, IBaseRepository } from '../../base/base.repository';
import { Serializable } from '../../../common/interfaces/serializable.interface';
import { Submission } from '../serializers/submission.serializer';
import { IProblem } from '../../problem/interfaces/problem.interface';
import { IUser } from 'src/pkg/user/interfaces/user.interface';
import { ISubmissionTestResult } from './submission-test-result.interface';

export interface ISubmission {
  id: number;
  source?: string;
  language: string;
  status: string;
  time?: number;
  memory?: number;
  score?: number;
  shared: boolean;
  problem: IProblem;
  problemId: IProblem['id'];
  createdBy: IUser;
  createdById: IUser['id'];
  testResults?: ISubmissionTestResult[];
}

export interface ISubmissionRepository
  extends IBaseRepository<ISubmission>,
    Serializable<ISubmission, Submission>,
    CanGetById<ISubmission> {}
