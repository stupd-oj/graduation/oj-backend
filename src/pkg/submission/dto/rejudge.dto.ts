import { IsIn, IsInt, IsString } from 'class-validator';
import { SubmissionStatus } from '../constants/submission-status';

export class RejudgeDTO {
  @IsInt()
  from: number;

  @IsInt()
  to: number;

  @IsInt()
  problemId: number;

  @IsString({
    each: true,
  })
  @IsIn(
    [
      SubmissionStatus.AC,
      SubmissionStatus.WA,
      SubmissionStatus.PAC,
      SubmissionStatus.TLE,
      SubmissionStatus.RTE,
    ],
    { each: true },
  )
  status: SubmissionStatus[];
}
