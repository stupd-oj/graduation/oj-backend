import { IsIn, IsInt, IsString, MaxLength } from 'class-validator';
import { LanguageCode } from '../constants/language-code';
import { ISubmission } from '../interfaces/submission.interface';

export class CreateSubmissionDTO implements Partial<ISubmission> {
  @IsInt()
  problemId: number;

  @IsString()
  @MaxLength(5000)
  source: string;

  @IsString()
  @IsIn(Object.values(LanguageCode))
  language: LanguageCode;
}
