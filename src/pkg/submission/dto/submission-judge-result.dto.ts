import { SubmissionTestResult } from '../serializers/submission-test-result.serializer';

export class SubmissionJudgeResult {
  submissionId: number;
  status: string;
  tests: SubmissionTestResult[];
}
