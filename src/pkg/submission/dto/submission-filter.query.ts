import { BaseFilter } from '../../base/dto/filter';
import { IsInt, IsOptional, IsString } from 'class-validator';
import { Number } from '../../../common/decorators/serializers/number.decorator';
import { PartialType } from '@nestjs/mapped-types';

export class SubmissionFilter extends PartialType(BaseFilter) {
  @Number()
  @IsInt()
  contestId: number;

  @IsOptional()
  @Number()
  @IsInt()
  problemId: number;

  @IsOptional()
  @Number()
  @IsInt()
  userId: number;

  @IsOptional()
  @IsString()
  status: string;
}
