import { IProblemTest } from '../../problem/interfaces/problem-test.interface';
import { ISubmission } from '../interfaces/submission.interface';

export class JudgeTaskDTO {
  submissionId: number;
  source: string;
  languageCode: string;
  timeLimit: number;
  memoryLimit: number;
  tests: IProblemTest[];

  private constructor(partial?: Partial<JudgeTaskDTO>) {
    Object.assign(this, partial);
  }

  static from(submission: ISubmission): JudgeTaskDTO {
    const { problem } = submission;
    return new JudgeTaskDTO({
      submissionId: submission.id,
      languageCode: submission.language,
      source: submission.source,
      timeLimit: problem?.timeLimit,
      memoryLimit: problem?.memoryLimit,
      tests: problem?.tests?.filter((e) => e.active),
    });
  }
}
