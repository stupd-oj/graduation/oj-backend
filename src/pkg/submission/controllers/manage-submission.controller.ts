import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  SerializeOptions,
} from '@nestjs/common';
import { Role } from '../../../common/decorators/metadata/role.decorator';
import { UserRole } from '../../user/constants/user-role';
import { SubmissionService } from '../submission.service';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { IManager } from '../../../common/interfaces/role.interface';
import { ResponseDTO } from '../../base/dto/response';
import { Submission } from '../serializers/submission.serializer';
import { anyTruthy } from '../../../common/helpers/array.helper';
import {
  AuthorityException,
  AuthorityType,
} from '../../../common/interfaces/manageable.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { CreateSubmissionDTO } from '../dto/submission.dto';
import { ProblemService } from '../../problem/problem.service';
import { RedisService } from '../../../providers/redis/redis.service';
import { SerialGroup } from '../../../common/constants/serial-group';

@ApiTags(API_TAG.MANAGE_SUBMISSION)
@Role(UserRole.Admin)
@Controller('admin/submissions')
@SerializeOptions({ groups: [SerialGroup.Admin] })
export class ManageSubmissionController {
  constructor(
    private readonly submissionService: SubmissionService,
    private readonly problemService: ProblemService,
    private readonly redisService: RedisService,
  ) {}

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) submissionId: number,
    @CurrentUser() user: IManager,
  ): Promise<ResponseDTO<Submission>> {
    const submission = await this.submissionService.findByIdOrFail(
      submissionId,
    );

    if (
      !anyTruthy(
        user.hasRole(UserRole.Admin),
        submission.problem.createdById === user.getIdentifier(),
      )
    ) {
      throw AuthorityException.of('problem', AuthorityType.read);
    }

    return ResponseDTO.ok(submission);
  }

  @Post()
  async createTestSubmission(
    @Body() dto: CreateSubmissionDTO,
    @CurrentUser() user: IManager,
  ): Promise<ResponseDTO<Submission>> {
    const problem = await this.problemService.findByIdOrFail(
      dto.problemId,
      true,
    );

    if (
      !anyTruthy(
        user.hasRole(UserRole.Admin),
        problem.createdById === user.getIdentifier(),
      )
    ) {
      throw AuthorityException.of('problem', AuthorityType.read);
    }

    const submission = await this.submissionService.createTestSubmission({
      ...dto,
      problem,
    });

    return ResponseDTO.ok(submission);
  }
}
