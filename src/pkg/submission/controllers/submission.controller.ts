import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { Response, ResponseDTO } from '../../base/dto/response';
import { SubmissionService } from '../submission.service';
import { CreateSubmissionDTO } from '../dto/submission.dto';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { UserRole } from '../../user/constants/user-role';
import { Role } from '../../../common/decorators/metadata/role.decorator';
import { RejudgeDTO } from '../dto/rejudge.dto';
import { SubmissionFilter } from '../dto/submission-filter.query';
import { ProblemService } from '../../problem/problem.service';
import { Submission } from '../serializers/submission.serializer';
import { IManager } from '../../../common/interfaces/role.interface';
import { anyTruthy } from '../../../common/helpers/array.helper';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { HttpException } from '../../../common/exceptions/http.exception';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { User } from '../../user/serializers/user.serializer';
import { EventPattern, Payload } from '@nestjs/microservices';
import { Event } from '../../../providers/amqp/constants/event';
import { SubmissionJudgeResult } from '../dto/submission-judge-result.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Event as InternalEvent } from '../../../providers/event/constants/event';
import { RedisService } from '../../../providers/redis/redis.service';

@ApiTags(API_TAG.SUBMISSION)
@Controller('submissions')
export class SubmissionController {
  constructor(
    private readonly submissionService: SubmissionService,
    private readonly problemService: ProblemService,
    private readonly redisService: RedisService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  @Get('')
  async filter(@Query() filter: SubmissionFilter) {
    return this.submissionService.filter(filter);
  }

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) submissionId: number,
    @CurrentUser() user: IManager,
  ): Promise<ResponseDTO<Submission>> {
    const submission = await this.submissionService.findByIdOrFail(
      submissionId,
    );

    if (
      !anyTruthy(
        submission.shared,
        submission.createdById === user.getIdentifier(),
      )
    ) {
      delete submission.source;
    }

    return ResponseDTO.ok(submission);
  }

  @Post('')
  async create(
    @Body() dto: CreateSubmissionDTO,
    @CurrentUser() user: IManager,
  ): Response<number> {
    const problem = await this.problemService.findByIdOrFail(
      dto.problemId,
      true,
    );
    if (
      !(await this.problemService.userCanSubmit(problem, user.getIdentifier()))
    )
      throw HttpException.forbidden(PreDefinedError.SUBMISSION_DENIED);

    const submission = await this.submissionService.create({
      ...dto,
      problem,
      createdBy: User.withId(user.getIdentifier()),
    });

    return ResponseDTO.ok(submission.id);
  }

  @Role(UserRole.Admin)
  @Post('rejudge')
  async rejudge(
    @Body() filter: RejudgeDTO,
    @CurrentUser() user: IManager,
  ): Promise<ResponseDTO<string>> {
    const problem = await this.problemService.findByIdOrFail(filter.problemId);

    problem.checkWriteAuthority(user);

    const numberOfSubmissions = await this.submissionService.rejudge(filter);
    return ResponseDTO.ok(`Rejudging ${numberOfSubmissions} submissions`);
  }

  @EventPattern(Event.UPDATE_JUDGE_TASK)
  async onUpdateJudgeTask(
    @Payload() result: SubmissionJudgeResult,
  ): Promise<void> {
    const { submissionId, status, tests } = result;
    if (submissionId < 0) {
      const submission = await this.submissionService.findByIdOrFail(
        submissionId,
      );
      if (submission) {
        if (tests) {
          submission.testResults = tests;
        }
        if (status) {
          submission.status = status;
        }
        await this.redisService.set(
          `submission@${submissionId}`,
          submission,
          600,
        );
      }
    } else {
      if (tests) {
        await this.submissionService.updateFinalResult(submissionId, tests);
      }
      if (status) {
        await this.submissionService.update(submissionId, { status });
      }
    }
  }
}
