import { Inject, Injectable, Logger } from '@nestjs/common';
import { SubmissionFilter } from './dto/submission-filter.query';
import { SubmissionRepository } from './repositories/submission.repository';
import { Response, ResponseDTO } from '../base/dto/response';
import { RejudgeDTO } from './dto/rejudge.dto';
import { ProblemService } from '../problem/problem.service';
import { ContestService } from '../contest/contest.service';
import { Between, In } from 'typeorm';
import { ContestRule } from '../contest/constants/contest-rule';
import { SubmissionTestResult } from './serializers/submission-test-result.serializer';
import { Submission } from './serializers/submission.serializer';
import {
  ISubmission,
  ISubmissionRepository,
} from './interfaces/submission.interface';
import {
  SubmissionStatus,
  SubmissionTestResultStatus,
} from './constants/submission-status';
import { ClientProxy } from '@nestjs/microservices';
import { AmqpQueue } from '../../providers/amqp/constants/queue';
import { Event } from '../../providers/amqp/constants/event';
import { Event as InternalEvent } from '../../providers/event/constants/event';
import { JudgeTaskDTO } from './dto/judge-task.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { omit } from 'lodash';
import { RedisService } from '../../providers/redis/redis.service';

export abstract class SubmissionService {
  abstract updateFinalResult(
    submissionId: number,
    testResults: SubmissionTestResult[],
  ): Promise<void>;

  abstract update(
    submissionId: number,
    partial: Partial<ISubmission>,
  ): Promise<void>;

  abstract findByIdOrFail(id: number): Promise<Submission>;

  abstract filter(filter: SubmissionFilter): Response<Submission[]>;

  abstract create(partial: Partial<ISubmission>): Promise<Submission>;

  abstract createTestSubmission(
    partial: Partial<ISubmission>,
  ): Promise<Submission>;

  abstract rejudge(filter: RejudgeDTO): Promise<number>;
}

@Injectable()
export class SubmissionServiceImpl extends SubmissionService {
  private readonly logger = new Logger(SubmissionService.name);

  constructor(
    @Inject(SubmissionRepository.provide)
    private readonly submissionRepository: ISubmissionRepository,
    private readonly problemService: ProblemService,
    private readonly contestService: ContestService,
    @Inject(AmqpQueue.JUDGER_SERVICE)
    private readonly judger: ClientProxy,
    private readonly eventEmitter: EventEmitter2,
    private readonly redisService: RedisService,
  ) {
    super();
  }

  async updateFinalResult(
    submissionId: number,
    testResults: SubmissionTestResult[],
  ): Promise<void> {
    try {
      const submission = await this.findByIdOrFail(submissionId);
      const { problem } = submission;
      const contest = await this.contestService.findByIdOrFail(
        problem.contestId,
      );
      const finalResult = this.getFinalResult(testResults, contest.rule);
      finalResult.score = problem.multiplier * finalResult.score ?? 0;
      await this.update(submissionId, finalResult);
      await this.findByIdOrFail(submissionId).then((_submission) =>
        this.eventEmitter.emit(
          InternalEvent.SUBMISSION_RESULT_UPDATED,
          _submission,
        ),
      );
    } catch (e) {
      this.logger.error(`Update submission result failed: ${submissionId}`);
      this.logger.error(e);
    }
  }

  private getFinalResult(
    testResults: SubmissionTestResult[],
    contestRule: ContestRule,
  ): Partial<Submission> {
    const MAX_SCORE = 100;
    const finalResult: Partial<Submission> = {
      score: MAX_SCORE,
      time: 0,
      memory: 0,
      status: SubmissionStatus.AC,
      testResults,
    };
    const numberOfTests = testResults?.length ?? 0;
    if (numberOfTests === 0) return finalResult;
    const numberOfAccepted = testResults.filter(
      (t) => t.result === SubmissionTestResultStatus.AC,
    ).length;
    finalResult.score = Math.round(100 * (numberOfAccepted / numberOfTests));
    finalResult.time = Math.max(...testResults.map((t) => t.time));
    finalResult.memory = Math.max(...testResults.map((t) => t.memory));

    if (finalResult.score === MAX_SCORE) return finalResult;
    if (contestRule === ContestRule.ACM) finalResult.score = 0;

    if (finalResult.score > 0) {
      finalResult.status = SubmissionStatus.PAC;
      return finalResult;
    }

    for (const status of [
      SubmissionTestResultStatus.IE,
      SubmissionTestResultStatus.RTE,
      SubmissionTestResultStatus.TLE,
      SubmissionTestResultStatus.WA,
    ]) {
      if (testResults.some((e) => e.result === status)) {
        finalResult.status = status;
        break;
      }
    }

    return finalResult;
  }

  async filter(filter: SubmissionFilter): Response<Submission[]> {
    const {
      userId,
      contestId,
      problemId,
      status,
      sort = [],
      limit,
      offset,
    } = filter;
    const queryBuilder = this.submissionRepository.createQueryBuilder('s');
    queryBuilder.leftJoinAndMapOne('s.problem', 's.problem', 'p');
    queryBuilder.leftJoinAndMapOne('s.createdBy', 's.createdBy', 'u');
    queryBuilder.where('p.contest_id = :contestId', { contestId });
    if (problemId) {
      queryBuilder.andWhere('p.id = :problemId', { problemId });
    }
    if (userId) {
      queryBuilder.andWhere('u.id = :userId', { userId });
    }
    if (status) {
      queryBuilder.andWhere('s.status = :status', { status });
    }

    for (const s of sort) {
      queryBuilder.orderBy(s.field, s.direction);
    }
    queryBuilder.take(limit);
    queryBuilder.skip(offset);
    const [entities, total] = await queryBuilder.getManyAndCount();
    return ResponseDTO.ok(
      this.submissionRepository.toSerializerMany(
        entities.map((e) => omit(e, 'source')),
      ),
      total,
    );
  }

  async create(partial: Partial<Submission>): Promise<Submission> {
    const submission = this.submissionRepository.create(partial);
    await this.submissionRepository.save(submission);

    this.postCreate(submission);
    return this.submissionRepository.toSerializer(submission);
  }

  findByIdOrFail(id: number): Promise<Submission> {
    if (id < 0) return this.findSubmissionTestById(id);
    return this.submissionRepository
      .findByIdOrFail(id)
      .then(this.submissionRepository.toSerializer);
  }

  findSubmissionTestById(id: number): Promise<Submission> {
    return this.redisService
      .get<Submission>(`submission@${id}`)
      .then(this.submissionRepository.toSerializer);
  }

  async update(id: number, partial: Partial<Submission>): Promise<void> {
    const submission = await this.submissionRepository.findByIdOrFail(id);
    this.submissionRepository.merge(submission, partial);
    await this.submissionRepository.save(submission);
    this.postUpdate(submission);
  }

  async rejudge(filter: RejudgeDTO): Promise<number> {
    const { problemId, status, from, to } = filter;
    const problem = await this.problemService.findByIdOrFail(problemId, true);
    const submissions = await this.submissionRepository.find({
      where: {
        id: Between(from, to),
        problemId,
        status: In(status),
      },
    });
    submissions.forEach((submission) => {
      this.judger.emit(
        Event.NEW_JUDGE_TASK,
        JudgeTaskDTO.from({
          ...submission,
          problem,
        }),
      );
    });
    return submissions.length;
  }

  private postCreate(submission: ISubmission): void {
    this.judger.emit(Event.NEW_JUDGE_TASK, JudgeTaskDTO.from(submission));
    this.eventEmitter.emit(InternalEvent.SUBMISSION_CREATED, submission);
  }

  private postUpdate(submission: ISubmission): void {
    this.eventEmitter.emit(InternalEvent.SUBMISSION_UPDATED, submission);
  }

  async createTestSubmission(
    partial: Partial<ISubmission>,
  ): Promise<Submission> {
    const submission = this.submissionRepository.create(partial);
    submission.id = -Date.now();

    this.judger.emit(Event.NEW_JUDGE_TASK, JudgeTaskDTO.from(submission));
    await this.redisService.set(`submission@${submission.id}`, submission, 600);
    return this.submissionRepository.toSerializer(submission);
  }
}
