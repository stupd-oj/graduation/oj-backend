export enum LanguageCode {
  C = 'C',
  CPP = 'CPP',
  JAVA = 'JAVA',
  PYTHON = 'PYTHON',
  NODE = 'NODE',
  PASCAL = 'PASCAL',
}
