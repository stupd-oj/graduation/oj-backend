export enum SubmissionStatus {
  IQ = 'IQ', // In Queue
  IP = 'IP', // In Processing
  AC = 'AC', // Accepted
  PAC = 'PAC', // Partial accepted
  WA = 'WA', // Wrong Answer
  CE = 'CE', // Compile Error
  RTE = 'RTE', // Runtime Exception
  TLE = 'TLE', // Time Limit Exceeded
  IE = 'IE', // Internal Error
  RJ = 'RJ', // Rejudge
}

export enum SubmissionTestResultStatus {
  AC = 'AC', // Accepted
  WA = 'WA', // Wrong Answer
  RTE = 'RTE', // Runtime Exception
  TLE = 'TLE', // Time Limit Exceeded
  IE = 'IE', // Internal Error
}
