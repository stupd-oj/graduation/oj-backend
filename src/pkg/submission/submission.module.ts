import { Module } from '@nestjs/common';
import { SubmissionService, SubmissionServiceImpl } from './submission.service';
import { SubmissionController } from './controllers/submission.controller';
import { ProblemModule } from '../problem/problem.module';
import { ContestModule } from '../contest/contest.module';
import { SubmissionRepository } from './repositories/submission.repository';
import { ManageSubmissionController } from './controllers/manage-submission.controller';

@Module({
  imports: [ProblemModule, ContestModule],
  providers: [
    SubmissionRepository,
    {
      provide: SubmissionService,
      useClass: SubmissionServiceImpl,
    },
  ],
  controllers: [SubmissionController, ManageSubmissionController],
  exports: [SubmissionService],
})
export class SubmissionModule {}
