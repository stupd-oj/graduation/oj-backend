import { ISubmissionTestResult } from '../interfaces/submission-test-result.interface';
import { ISubmission } from '../interfaces/submission.interface';
import { Exclude } from 'class-transformer';

export class SubmissionTestResult implements ISubmissionTestResult {
  memory: number;
  result: string;
  @Exclude()
  submission: ISubmission;
  submissionId: ISubmission['id'];
  testUuid: string;
  time: number;
}
