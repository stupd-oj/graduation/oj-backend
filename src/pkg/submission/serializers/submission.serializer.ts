import { ISubmission } from '../interfaces/submission.interface';
import { BaseModel } from '../../base/base.model';
import { SubmissionTestResult } from './submission-test-result.serializer';
import { Expose, Type } from 'class-transformer';
import { ISubmissionTestResult } from '../interfaces/submission-test-result.interface';
import { IProblem } from '../../problem/interfaces/problem.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { Pick } from '../../../common/decorators/serializers/pick.decorator';
import { SerialGroup } from '../../../common/constants/serial-group';

export class Submission extends BaseModel implements ISubmission {
  createdById: IUser['id'];
  id: number;
  language: string;
  memory: number;
  problemId: IProblem['id'];
  score: number;
  shared: boolean;
  source?: string;
  status: string;
  time: number;

  @Pick('id', 'username', 'fullName')
  createdBy: IUser;

  @Pick('id', 'code', 'name', 'contestId')
  problem: IProblem;

  @Expose({ groups: [SerialGroup.Admin] })
  @Type(() => SubmissionTestResult)
  testResults?: ISubmissionTestResult[];

  constructor(partial?: Partial<ISubmission>) {
    super();
    Object.assign(this, partial);
  }
}
