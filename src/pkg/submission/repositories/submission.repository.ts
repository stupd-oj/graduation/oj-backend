import { SubmissionEntity } from '../../../entities/sql/submission.entity';
import { DataSource, FindOneOptions } from 'typeorm';
import {
  ISubmission,
  ISubmissionRepository,
} from '../interfaces/submission.interface';
import { Submission } from '../serializers/submission.serializer';
import { HttpException } from '../../../common/exceptions/http.exception';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const SubmissionRepository: FactoryProvider<ISubmissionRepository> = {
  provide: 'SubmissionRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): ISubmissionRepository => {
    const extended: Partial<ISubmissionRepository> = {
      toSerializer(plain: ISubmission): Submission {
        return new Submission(plain);
      },

      toSerializerMany(plain: ISubmission[]): Submission[] {
        return plain.map(this.toSerializer);
      },

      findById(id: number): Promise<ISubmission> {
        return this.findOneBy({ id });
      },

      findByIdOrFail(id: number): Promise<ISubmission> {
        return this.findOneByOrFail({ id }).catch(() => {
          throw HttpException.notFound(PreDefinedError.SUBMISSION_NOT_FOUND);
        });
      },
    };
    return datasource
      .getRepository(SubmissionEntity)
      .extend(extended as ISubmissionRepository);
  },
};
