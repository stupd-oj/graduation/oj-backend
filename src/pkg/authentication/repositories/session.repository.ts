import { DataSource } from 'typeorm';
import { SessionEntity } from '../../../entities/sql/session.entity';
import { ISessionRepository } from '../interfaces/session.interface';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const SessionRepository: FactoryProvider<ISessionRepository> = {
  provide: 'SessionRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): ISessionRepository => {
    const extended: Partial<ISessionRepository> = {};
    return datasource
      .getRepository(SessionEntity)
      .extend(extended as ISessionRepository);
  },
};
