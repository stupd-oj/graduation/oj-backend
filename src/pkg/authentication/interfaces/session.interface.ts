import { IBaseRepository } from '../../base/base.repository';
import { IUser } from '../../user/interfaces/user.interface';

export interface ISession {
  uuid: string;
  lastActive: Date;
  refreshToken: string;
  userAgent: string;
  ipAddress: string;
  expireAt: Date;
  user: IUser;
  userId: IUser['id'];
  createdAt: Date;
}

export interface ISessionRepository extends IBaseRepository<ISession> {}
