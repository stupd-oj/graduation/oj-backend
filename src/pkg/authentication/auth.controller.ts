import { Body, Controller, Post, Req } from '@nestjs/common';
import { RegisterRequest } from './dto/register.dto';
import { LoginRequest } from './dto/login.dto';
import { Public } from '../../common/decorators/metadata/public.decorator';
import { OtpVerifyDTO } from './dto/otp-verify.dto';
import { UpdatePasswordDTO } from './dto/update-password.dto';
import { Response, ResponseDTO } from '../base/dto/response';
import { AuthService } from './auth.service';
import { RefreshAccessTokenRequest } from './dto/refresh-access-token.dto';
import { OtpRequestDTO } from './dto/otp-request.dto';
import { Request } from 'express';
import { LogoutRequest } from './dto/logout.dto';
import { UserService } from '../user/user.service';
import { LoginResponse } from './serializers/login-response.serializer';
import { plainToInstance } from 'class-transformer';

@Public()
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('register')
  async register(@Body() dto: RegisterRequest): Response {
    await this.authService.register(dto);
    return ResponseDTO.ok();
  }

  @Post('login')
  async login(
    @Req() request: Request,
    @Body() dto: LoginRequest,
  ): Response<LoginResponse> {
    const ip = request.get('x-real-ip') ?? '';
    const userAgent = request.get('user-agent');
    return this.authService.login(
      plainToInstance(LoginRequest, dto),
      ip,
      userAgent,
    );
  }

  @Post('refresh-token')
  async refreshAccessToken(
    @Body() { refreshToken }: RefreshAccessTokenRequest,
  ) {
    return this.authService.refreshAccessToken(refreshToken);
  }

  @Post('check-identifier-availability')
  async checkIdentifierAvailability(
    @Body() dto: OtpRequestDTO,
  ): Promise<ResponseDTO<boolean>> {
    return ResponseDTO.ok(
      await this.userService.checkIdentifierAvailability(dto.identifier),
    );
  }

  @Post('account-verify/otp')
  async requestOtpVerifyAccount(
    @Body() dto: OtpRequestDTO,
  ): Promise<ResponseDTO> {
    await this.authService.requestOTPForAccountVerification(dto.identifier);
    return ResponseDTO.ok();
  }

  @Post('account-verify')
  async verifyAccount(@Body() dto: OtpVerifyDTO): Promise<ResponseDTO> {
    await this.authService.verifyAccount(dto);
    return ResponseDTO.ok();
  }

  @Post('password-recover/otp')
  async requestOtpForgotPassword(@Body() dto: OtpRequestDTO) {
    await this.authService.requestOTPForPasswordRecover(dto.identifier);
    return ResponseDTO.ok();
  }

  @Post('password-recover')
  async updatePasswordWithOtp(@Body() dto: UpdatePasswordDTO) {
    await this.authService.updatePassword(dto);
    return ResponseDTO.ok();
  }

  @Post('logout')
  async logout(@Body() dto: LogoutRequest) {
    await this.authService.logout(dto);
    return ResponseDTO.ok();
  }
}
