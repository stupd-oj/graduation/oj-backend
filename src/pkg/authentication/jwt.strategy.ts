import { Strategy, ExtractJwt } from 'passport-jwt';
import { Inject, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { IUser } from '../user/interfaces/user.interface';
import { User } from '../user/serializers/user.serializer';
import { IJwtConfig } from '../../config/jwt/jwt-config.interface';
import { JwtConfig } from '../../config/jwt/jwt.config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@Inject(JwtConfig) jwtConfig: IJwtConfig) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: jwtConfig.secret,
      issuer: jwtConfig.issuer,
      ignoreExpiration: false,
    });
  }

  validate(payload: IUser): IUser {
    return payload ? new User(payload) : User.withId(-1); // ID -1 mean anonymous
  }
}
