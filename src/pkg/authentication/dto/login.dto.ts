import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class LoginRequest {
  @Transform(({ value }) => value?.toLowerCase())
  @IsString()
  identifier: string;

  @IsString()
  password: string;
}
