import { IsString } from 'class-validator';

export class RefreshAccessTokenRequest {
  @IsString()
  refreshToken: string;
}
