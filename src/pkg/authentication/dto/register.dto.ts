import { IsAlphanumeric, IsEmail, IsString, Length } from 'class-validator';
import { Transform } from 'class-transformer';
import { IsVietnamese } from '../../../common/decorators/validations/is-vietnamese.decorator';
import { ProhibitedKeywords } from '../../../common/decorators/validations/prohibited-keywords.decorator';
import { IUser } from '../../user/interfaces/user.interface';

export class RegisterRequest implements Partial<IUser> {
  @IsString()
  @Transform(({ value }) => value?.toLowerCase())
  @ProhibitedKeywords('admin', 'itptit')
  @Length(3, 30)
  @IsAlphanumeric()
  username: string;

  @IsString()
  @Transform(({ value }) => value?.toLowerCase())
  @Length(0, 255)
  @IsEmail()
  email: string;

  @IsString()
  @IsVietnamese()
  @Length(0, 30)
  fullName: string;

  @IsString()
  @Length(6)
  password: string;
}
