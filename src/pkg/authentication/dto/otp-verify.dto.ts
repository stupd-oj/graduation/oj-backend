import { IsNumberString, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class OtpVerifyDTO {
  @Transform(({ value }) => value?.toLowerCase())
  @IsString()
  identifier: string;

  @IsNumberString()
  otp: string;
}
