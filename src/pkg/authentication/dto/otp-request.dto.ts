import { Transform } from 'class-transformer';
import { IsString } from 'class-validator';

export class OtpRequestDTO {
  @Transform(({ value }) => value?.toLowerCase())
  @IsString()
  identifier: string;
}
