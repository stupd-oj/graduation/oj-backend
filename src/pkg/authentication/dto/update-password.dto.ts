import { IsString, Length } from 'class-validator';
import { OtpVerifyDTO } from './otp-verify.dto';

export class UpdatePasswordDTO extends OtpVerifyDTO {
  @IsString()
  @Length(6)
  password: string;
}
