import { IsOptional, IsString } from 'class-validator';

export class LogoutRequest {
  @IsString()
  refreshToken: string;

  @IsOptional()
  @IsString()
  sessionId: string;
}
