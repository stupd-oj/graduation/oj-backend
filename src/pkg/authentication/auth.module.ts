import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt.strategy';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { AuthService, AuthServiceImpl } from './auth.service';
import { UserModule } from '../user/user.module';
import { JwtConfigModule } from '../../config/jwt/jwt-config.module';
import { JwtConfig } from '../../config/jwt/jwt.config';
import { IJwtConfig } from '../../config/jwt/jwt-config.interface';
import { SessionRepository } from './repositories/session.repository';

@Module({
  imports: [
    JwtConfigModule,
    JwtModule.registerAsync({
      imports: [JwtConfigModule],
      inject: [JwtConfig],
      useFactory: (jwtConfig: IJwtConfig): JwtModuleOptions => ({
        secret: jwtConfig.secret,
        signOptions: {
          issuer: jwtConfig.issuer,
          expiresIn: jwtConfig.expiration,
        },
        verifyOptions: {
          issuer: jwtConfig.issuer,
        },
      }),
    }),
    UserModule,
  ],
  controllers: [AuthController],
  providers: [
    JwtStrategy,
    SessionRepository,
    {
      provide: AuthService,
      useClass: AuthServiceImpl,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
