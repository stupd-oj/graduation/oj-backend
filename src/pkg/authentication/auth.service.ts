import { Inject, Injectable } from '@nestjs/common';
import { LoginRequest } from './dto/login.dto';
import { RegisterRequest } from './dto/register.dto';
import { MoreThan } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { OtpVerifyDTO } from './dto/otp-verify.dto';
import { UpdatePasswordDTO } from './dto/update-password.dto';
import { HttpException } from '../../common/exceptions/http.exception';
import { PreDefinedError } from '../../common/constants/pre-defined-error';
import { padStart } from 'lodash';
import * as crypto from 'crypto';
import { instanceToPlain } from 'class-transformer';
import { Response, ResponseDTO } from '../base/dto/response';
import { RedisService } from '../../providers/redis/redis.service';
import { LogoutRequest } from './dto/logout.dto';
import { SessionRepository } from './repositories/session.repository';
import { ISession, ISessionRepository } from './interfaces/session.interface';
import { OtpType } from './constants/otp-type';
import { UserService } from '../user/user.service';
import { IUser } from '../user/interfaces/user.interface';
import { LoginResponse } from './serializers/login-response.serializer';
import { RefreshAccessTokenResponse } from './serializers/refresh-access-token-response.serializer';
import dateHelper from '../../common/helpers/date.helper';
import { MailJob } from '../../providers/queue/constants/job';
import { MailProducer } from '../../providers/queue/producers/mail.producer';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Event } from '../../providers/event/constants/event';

const { createHash, randomBytes, randomInt } = crypto;

export abstract class AuthService {
  abstract login(
    loginRequest: LoginRequest,
    ipAddress: string,
    userAgent: string,
  ): Response<LoginResponse>;

  abstract register(dto: RegisterRequest): Promise<ResponseDTO>;

  abstract refreshAccessToken(
    refreshToken: string,
  ): Response<RefreshAccessTokenResponse>;

  abstract requestOTPForAccountVerification(
    identifier: string | number,
  ): Promise<void>;

  abstract requestOTPForPasswordRecover(
    identifier: string | number,
  ): Promise<void>;

  abstract verifyAccount(dto: OtpVerifyDTO): Promise<void>;

  abstract updatePassword(dto: UpdatePasswordDTO): Promise<void>;

  abstract logout(dto: LogoutRequest): Promise<void>;
}

@Injectable()
export class AuthServiceImpl extends AuthService {
  private readonly SESSION_LIFETIME = 30; // Unit: days
  private readonly OTP_LIFETIME = 15 * 60; // Unit: minutes

  constructor(
    @Inject(SessionRepository.provide)
    private readonly sessionRepository: ISessionRepository,
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly redisService: RedisService,
    private readonly mailProducer: MailProducer,
    private readonly eventEmitter: EventEmitter2,
  ) {
    super();
  }

  private generateJwtToken(payload: IUser): string {
    return this.jwtService.sign(instanceToPlain(payload));
  }

  private static generateOneTimePassword(): string {
    return padStart(randomInt(0, 999999).toString(), 6, '0');
  }

  private static generateRefreshToken(): string {
    return createHash('sha256').update(randomBytes(32)).digest('hex');
  }

  private async createSession(
    user: IUser,
    ipAddress: string,
    userAgent: string,
  ): Promise<ISession> {
    const session = this.sessionRepository.create({
      user,
      ipAddress,
      userAgent,
      refreshToken: AuthServiceImpl.generateRefreshToken(),
      expireAt: dateHelper.plus(Date.now(), this.SESSION_LIFETIME, 'days'),
      lastActive: new Date(),
    });
    await this.sessionRepository.save(session);
    return session;
  }

  private async createOneTimePassword(
    user: IUser,
    type: OtpType,
  ): Promise<string> {
    const code = AuthServiceImpl.generateOneTimePassword();
    await this.redisService.set(
      `otp@${user.id}/${type}`,
      code,
      this.OTP_LIFETIME,
    );
    return code;
  }

  private async getOTPOrCreateIfNotExisted(
    user: IUser,
    type: OtpType,
  ): Promise<string> {
    const otp: string = await this.redisService.get(`otp@${user.id}/${type}`);
    if (!otp) return this.createOneTimePassword(user, type);
    return otp;
  }

  private async validateAndInvokeOTP(
    code: string,
    user: IUser,
    type: OtpType,
  ): Promise<void> {
    const otp: string = await this.redisService.get(`otp@${user.id}/${type}`);

    if (code !== otp)
      throw HttpException.badRequest(PreDefinedError.OTP_INCORRECT);
    await this.redisService.del(`otp@${user.id}/${type}`);
  }

  async login(
    loginRequest: LoginRequest,
    ipAddress: string,
    userAgent: string,
  ): Response<LoginResponse> {
    const user = await this.userService.authenticate(
      loginRequest.identifier,
      loginRequest.password,
    );

    if (!user.isVerified) {
      throw HttpException.badRequest(PreDefinedError.ACCOUNT_UNVERIFIED);
    }

    const session = await this.createSession(user, ipAddress, userAgent);
    const accessToken = this.generateJwtToken(user);
    return ResponseDTO.ok({
      user,
      accessToken,
      refreshToken: session.refreshToken,
    });
  }

  async register(registerRequest: RegisterRequest): Promise<ResponseDTO> {
    const { username, email } = registerRequest;
    const [isUserNameAvailable, isEmailAvailable] = await Promise.all([
      this.userService.checkIdentifierAvailability(username),
      this.userService.checkIdentifierAvailability(email),
    ]);
    if (!isUserNameAvailable)
      throw HttpException.badRequest(PreDefinedError.REGISTER_USERNAME_EXISTED);
    if (!isEmailAvailable)
      throw HttpException.badRequest(PreDefinedError.REGISTER_EMAIL_EXISTED);

    const user = await this.userService.create(registerRequest);
    const code = await this.createOneTimePassword(
      user,
      OtpType.ACCOUNT_VERIFICATION,
    );
    await this.mailProducer.send(MailJob.ACCOUNT_VERIFICATION, {
      email: user.email,
      name: user.fullName,
      code,
    });
    return ResponseDTO.ok();
  }

  async refreshAccessToken(
    refreshToken: string,
  ): Response<RefreshAccessTokenResponse> {
    const session = await this.sessionRepository.findOneBy({
      refreshToken: refreshToken,
      expireAt: MoreThan(new Date()),
    });
    if (!session)
      throw HttpException.badRequest(PreDefinedError.REFRESH_TOKEN_INCORRECT);
    const accessToken = this.generateJwtToken(session.user);
    session.lastActive = new Date();
    await this.sessionRepository.save(session);
    return ResponseDTO.ok({ accessToken });
  }

  async requestOTPForAccountVerification(identifier: string): Promise<void> {
    const user = await this.userService.findByIdentifierOrFail(identifier);
    if (user.isVerified)
      throw HttpException.badRequest(PreDefinedError.ACCOUNT_VERIFIED);

    const code = await this.getOTPOrCreateIfNotExisted(
      user,
      OtpType.ACCOUNT_VERIFICATION,
    );

    await this.mailProducer.send(MailJob.ACCOUNT_VERIFICATION, {
      email: user.email,
      name: user.fullName,
      code,
    });
  }

  async requestOTPForPasswordRecover(identifier: string): Promise<void> {
    const user = await this.userService.findByIdentifierOrFail(identifier);
    const code = await this.getOTPOrCreateIfNotExisted(
      user,
      OtpType.PASSWORD_RECOVER,
    );

    await this.mailProducer.send(MailJob.PASSWORD_RECOVER, {
      email: user.email,
      name: user.fullName,
      code: code,
    });
  }

  async updatePassword({
    otp,
    identifier,
    password,
  }: UpdatePasswordDTO): Promise<void> {
    const user = await this.userService.findByIdentifierOrFail(identifier);
    await this.validateAndInvokeOTP(otp, user, OtpType.PASSWORD_RECOVER);
    await this.userService.updatePassword(user.id, password);
  }

  async verifyAccount({ otp, identifier }: OtpVerifyDTO): Promise<void> {
    const user = await this.userService.findByIdentifierOrFail(identifier);
    if (user.isVerified) {
      throw HttpException.badRequest(PreDefinedError.ACCOUNT_VERIFIED);
    }

    await this.validateAndInvokeOTP(otp, user, OtpType.ACCOUNT_VERIFICATION);

    await this.userService.update(user.id, { isVerified: true });

    this.postVerify(user);
  }

  logout(dto: LogoutRequest): Promise<void> {
    throw new Error('Not implemented yet');
  }

  postVerify(user: IUser): void {
    this.eventEmitter.emit(Event.USER_VERIFIED, user);
  }
}
