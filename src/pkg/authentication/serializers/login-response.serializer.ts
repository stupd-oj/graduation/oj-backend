import { Type } from 'class-transformer';
import { User } from '../../user/serializers/user.serializer';
import { IUser } from '../../user/interfaces/user.interface';

export class LoginResponse {
  accessToken: string;
  refreshToken: string;
  @Type(() => User)
  user: IUser;
}
