import { BaseFilter } from '../../base/dto/filter';
import { AcceptedSortField } from '../../../common/decorators/validations/accepted-sort-field.decorator';

export class ContestFilter extends BaseFilter {
  @AcceptedSortField(
    'id',
    'name',
    'rule',
    'startAt',
    'endAt',
    'closeRankAt',
    'createdAt',
  )
  sort: BaseFilter['sort'];
}
