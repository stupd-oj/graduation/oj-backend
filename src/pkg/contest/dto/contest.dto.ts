import {
  IsDateString,
  IsIn,
  IsOptional,
  IsString,
  IsUrl,
  Length,
  MaxLength,
} from 'class-validator';
import { ContestRule } from '../constants/contest-rule';
import { IContest } from '../interfaces/contest.interface';

export class ContestDTO implements Partial<IContest> {
  @IsString()
  @Length(1, 255)
  name: string;

  @IsString()
  @MaxLength(5000)
  description: string;

  @IsOptional()
  @IsString()
  image: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsString()
  @IsIn(Object.values(ContestRule))
  rule: ContestRule;

  @IsDateString()
  startAt: Date;

  @IsDateString()
  endAt: Date;

  @IsOptional()
  @IsDateString()
  closeRankAt: Date;
}
