import { DataSource } from 'typeorm';
import { ContestantEntity } from '../../../entities/sql/contestant.entity';
import {
  IContestant,
  IContestantRepository,
} from '../interfaces/contestant.interface';
import { Contestant } from '../serializers/contestant.serializer';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const ContestantRepository: FactoryProvider<IContestantRepository> = {
  provide: 'ContestantRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): IContestantRepository => {
    const extended: Partial<IContestantRepository> = {
      toSerializer(plain: IContestant): Contestant {
        return new Contestant(plain);
      },

      toSerializerMany(plain: IContestant[]): Contestant[] {
        return plain.map(this.toSerializer);
      },
    };
    return datasource
      .getRepository(ContestantEntity)
      .extend(extended as IContestantRepository);
  },
};
