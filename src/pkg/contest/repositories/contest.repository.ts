import { DataSource } from 'typeorm';
import { ContestEntity } from '../../../entities/sql/contest.entity';
import { IContest, IContestRepository } from '../interfaces/contest.interface';
import { Contest } from '../serializers/contest.serializer';
import { HttpException } from '../../../common/exceptions/http.exception';
import { PreDefinedError } from '../../../common/constants/pre-defined-error';
import { FactoryProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DataSourceToken } from '../../../providers/database/datasource-token';

export const ContestRepository: FactoryProvider<IContestRepository> = {
  provide: 'ContestRepository',
  inject: [DataSourceToken.primary],
  useFactory: (datasource: DataSource): IContestRepository => {
    const extended: Partial<IContestRepository> = {
      findByIdOrFail(id: number): Promise<IContest> {
        return this.findOneByOrFail({ id }).catch(() => {
          throw HttpException.notFound(PreDefinedError.CONTEST_NOT_FOUND);
        });
      },

      findById(id: number): Promise<IContest> {
        return this.findOneBy({ id });
      },

      toSerializer(plain: IContest): Contest {
        return new Contest(plain as unknown);
      },

      toSerializerMany(plain: IContest[]): Contest[] {
        return plain.map(this.toSerializer);
      },
    };
    return datasource
      .getRepository(ContestEntity)
      .extend(extended as IContestRepository);
  },
};
