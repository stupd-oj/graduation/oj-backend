import { IContest } from './contest.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { IBaseRepository } from '../../base/base.repository';
import { Contestant } from '../serializers/contestant.serializer';
import { Serializable } from '../../../common/interfaces/serializable.interface';

export interface IContestant {
  contest?: IContest;
  contestId: IContest['id'];
  user: IUser;
  userId: IUser['id'];
  createdAt: Date;
}

export interface IContestantRepository
  extends IBaseRepository<IContestant>,
    Serializable<IContestant, Contestant> {}
