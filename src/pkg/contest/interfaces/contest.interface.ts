import { IUser } from '../../user/interfaces/user.interface';
import { CanGetById, IBaseRepository } from '../../base/base.repository';
import { Serializable } from '../../../common/interfaces/serializable.interface';
import { Contest } from '../serializers/contest.serializer';

export interface IContest {
  id: number;
  name: string;
  description: string;
  image?: string;
  rule: string;
  startAt: Date;
  endAt: Date;
  closeRankAt: Date;
  updatedRankAt?: Date;
  password: string;
  createdBy: IUser;
  createdById: IUser['id'];
  joined?: boolean;
  numberOfContestants?: number;
  contestants?: any[];
}

export interface IContestRepository
  extends IBaseRepository<IContest>,
    Serializable<IContest, Contest>,
    CanGetById<IContest> {}
