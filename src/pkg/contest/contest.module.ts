import { Module } from '@nestjs/common';
import { ContestController } from './controllers/contest.controller';
import { ContestService, ContestServiceImpl } from './contest.service';
import { ContestRepository } from './repositories/contest.repository';
import { ContestantRepository } from './repositories/contestant.repository';
import { ManageContestController } from './controllers/manage-contest.controller';

@Module({
  controllers: [ContestController, ManageContestController],
  providers: [
    ContestRepository,
    ContestantRepository,
    {
      provide: ContestService,
      useClass: ContestServiceImpl,
    },
  ],
  exports: [ContestService],
})
export class ContestModule {}
