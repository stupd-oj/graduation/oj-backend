import { Inject, Injectable } from '@nestjs/common';
import { ContestFilter } from './dto/contest-filter.query';
import { ContestRepository } from './repositories/contest.repository';
import { HttpException } from '../../common/exceptions/http.exception';
import { Response, ResponseDTO } from '../base/dto/response';
import { PreDefinedError } from '../../common/constants/pre-defined-error';
import { IContest, IContestRepository } from './interfaces/contest.interface';
import { Contest } from './serializers/contest.serializer';
import {
  IContestant,
  IContestantRepository,
} from './interfaces/contestant.interface';
import { ContestantRepository } from './repositories/contestant.repository';
import { BaseFilter } from '../base/dto/filter';
import { allTruthy } from '../../common/helpers/array.helper';
import { plainToInstance } from 'class-transformer';
import { Contestant } from './serializers/contestant.serializer';
import { ContestConstant } from './constants/contest.constant';
import {
  exactFindOptions,
  mergeWhere,
} from '../../common/helpers/typeorm.helper';
import { FindManyOptions, ILike, In } from 'typeorm';

export abstract class ContestService {
  abstract filter(query: ContestFilter): Response<Contest[]>;

  abstract findRankUpdatableIds(): Promise<number[]>;

  abstract findByIdOrFail(id: number): Promise<Contest>;

  abstract joinContest(
    contestId: number,
    userId: number,
    password: string,
  ): Promise<void>;

  abstract isContestant(contestId: number, userId: number): Promise<boolean>;

  abstract getNumberOfContestants(contestId: number): Promise<number>;

  abstract create(partial: Partial<IContest>): Promise<Contest>;

  abstract update(contestId: number, partial: Partial<IContest>): Promise<void>;

  abstract remove(contestId: number): Promise<void>;

  abstract filterContestants(
    contestId: number,
    filter: BaseFilter,
  ): Response<IContestant[]>;

  abstract getContestJoinedByUser(userId: number): Promise<IContestant[]>;
}

@Injectable()
export class ContestServiceImpl extends ContestService {
  constructor(
    @Inject(ContestRepository.provide)
    private readonly contestRepository: IContestRepository,
    @Inject(ContestantRepository.provide)
    private readonly contestantRepository: IContestantRepository,
  ) {
    super();
  }

  async filter(filter: ContestFilter): Response<Contest[]> {
    const findOptions = exactFindOptions<IContest>(filter);
    mergeWhere(
      findOptions,
      { name: ILike(`%${filter.query}%`) },
      { rule: ILike(`%${filter.query}%`) },
    );
    const [entities, total] = await this.contestRepository.findAndCount({
      ...findOptions,
      loadRelationIds: {
        relations: ['contestants'],
      },
    });

    entities.forEach((e) => (e.numberOfContestants = e.contestants.length));

    return ResponseDTO.ok(
      this.contestRepository.toSerializerMany(entities),
      total,
    );
  }

  async findRankUpdatableIds(): Promise<number[]> {
    const query = this.contestRepository.createQueryBuilder('c');
    query.where('c.updated_rank_at IS NULL');
    query.orWhere('c.close_rank_at IS NULL');
    query.orWhere('c.updated_rank_at < c.close_rank_at');
    return query.getMany().then((rows) => rows.map((r) => r.id));
  }

  async isContestant(contestId: number, userId: number): Promise<boolean> {
    return (
      contestId === ContestConstant.DEFAULT_ID ||
      !!(await this.contestantRepository.countBy({ contestId, userId }))
    );
  }

  async getNumberOfContestants(contestId: number): Promise<number> {
    return await this.contestantRepository.countBy({ contestId });
  }

  private async addContestant(
    contestId: number,
    userId: number,
  ): Promise<void> {
    await this.contestantRepository.save({ contestId, userId });
  }

  async findByIdOrFail(id: number): Promise<Contest> {
    return await this.contestRepository
      .findByIdOrFail(id)
      .then(this.contestRepository.toSerializer);
  }

  async joinContest(contestId: number, userId: number, password: string) {
    const contest = await this.findByIdOrFail(contestId);

    if (allTruthy(contest.isPrivate, contest.password !== password)) {
      throw HttpException.badRequest(
        PreDefinedError.CONTEST_INCORRECT_PASSWORD,
      );
    }

    if (new Date() > contest.endAt)
      throw HttpException.badRequest(PreDefinedError.CONTEST_ENDED);

    const isContestant = await this.isContestant(contestId, userId);

    if (isContestant)
      throw HttpException.badRequest(
        PreDefinedError.CONTEST_ALREADY_CONTESTANT,
      );

    await this.addContestant(contestId, userId);
  }

  async create(partial: Partial<IContest>): Promise<Contest> {
    const contest = this.contestRepository.create(partial);
    if (!contest.closeRankAt) contest.closeRankAt = contest.endAt;
    await this.contestRepository.save(contest);
    return this.contestRepository.toSerializer(contest);
  }

  async update(id: number, partial: Partial<IContest>): Promise<void> {
    const contest = await this.contestRepository.findByIdOrFail(id);
    this.contestRepository.merge(contest, partial);
    if (!contest.closeRankAt) contest.closeRankAt = contest.endAt;
    await this.contestRepository.save(contest);
  }

  async remove(id: number): Promise<void> {
    const contest = await this.contestRepository.findByIdOrFail(id);
    if (contest.startAt < new Date())
      throw HttpException.badRequest(
        PreDefinedError.CANNOT_REMOVE_STARTED_CONTEST,
      );
    await this.contestRepository.remove(contest);
  }

  async filterContestants(
    contestId: number,
    filter: BaseFilter,
  ): Response<IContestant[]> {
    const [contestant, total] = await this.contestantRepository.findAndCount({
      where: { contest: { id: contestId } },
      skip: filter.offset,
      take: filter.limit,
    });
    return ResponseDTO.ok(
      this.contestantRepository.toSerializerMany(contestant),
      total,
    );
  }

  async getContestJoinedByUser(userId: number): Promise<IContestant[]> {
    const contestants = await this.contestantRepository.find({
      where: { user: { id: userId } },
      order: { createdAt: 'DESC' },
    });
    const contestIds = contestants.map((c) => c.contestId);
    const contests = await this.contestRepository.find({
      where: { id: In(contestIds) },
      loadRelationIds: {
        relations: ['contestants'],
      },
    });
    const contestMap = new Map<number, IContest>();
    contests.forEach((c) => {
      c.numberOfContestants = c.contestants.length;
      contestMap.set(c.id, c);
    });
    contestants.forEach((c) => (c.contest = contestMap.get(c.contestId)));
    return plainToInstance(
      Contestant,
      contestants.sort(
        (a, b) => b.contest.endAt.getTime() - a.contest.endAt.getTime(),
      ),
    );
  }
}
