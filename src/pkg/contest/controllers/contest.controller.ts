import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { ContestService } from '../contest.service';
import { Response, ResponseDTO } from '../../base/dto/response';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { ContestFilter } from '../dto/contest-filter.query';
import { IContest } from '../interfaces/contest.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { BaseFilter } from '../../base/dto/filter';
import { IContestant } from '../interfaces/contestant.interface';

@ApiTags(API_TAG.CONTEST)
@Controller('contests')
export class ContestController {
  constructor(private readonly contestService: ContestService) {}

  @Get('')
  async filter(@Query() query: ContestFilter): Response<IContest[]> {
    return this.contestService.filter(query);
  }

  @Get('joined')
  async getJoinedContests(
    @CurrentUser() user: IManager,
  ): Response<IContestant[]> {
    const contests = await this.contestService.getContestJoinedByUser(
      user.getIdentifier(),
    );
    return ResponseDTO.ok(contests);
  }

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) contestId: number,
    @CurrentUser() user?: IManager,
  ): Response<IContest> {
    const contest = await this.contestService.findByIdOrFail(contestId);
    if (user) {
      contest.joined = await this.contestService.isContestant(
        contestId,
        user?.getIdentifier(),
      );
    }

    contest.joinable = !contest.joined && !contest.isPrivate;
    contest.numberOfContestants =
      await this.contestService.getNumberOfContestants(contestId);
    return ResponseDTO.ok(contest);
  }

  @Get(':id/contestants')
  async getContestantsById(
    @Param('id', ParseIntPipe) contestId: number,
    @Query() filter: BaseFilter,
  ): Response<IContestant[]> {
    return this.contestService.filterContestants(contestId, filter);
  }

  @Post(':id/join')
  async join(
    @Body('password') password: string,
    @Param('id', ParseIntPipe) contestId: number,
    @CurrentUser() user: IManager,
  ): Response {
    await this.contestService.joinContest(
      contestId,
      user.getIdentifier(),
      password,
    );
    return ResponseDTO.ok();
  }
}
