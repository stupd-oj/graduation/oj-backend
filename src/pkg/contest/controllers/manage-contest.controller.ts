import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  SerializeOptions,
} from '@nestjs/common';
import { Role } from '../../../common/decorators/metadata/role.decorator';
import { Response, ResponseDTO } from '../../base/dto/response';
import { CurrentUser } from '../../../common/decorators/requests/current-user.decorator';
import { ContestService } from '../contest.service';
import { UserRole } from '../../user/constants/user-role';
import { ContestFilter } from '../dto/contest-filter.query';
import { IContest } from '../interfaces/contest.interface';
import { ContestDTO } from '../dto/contest.dto';
import { IManager } from '../../../common/interfaces/role.interface';
import { ApiTags } from '@nestjs/swagger';
import { API_TAG } from '../../../common/constants/api-tag';
import { User } from '../../user/serializers/user.serializer';
import { SerialGroup } from '../../../common/constants/serial-group';

@ApiTags(API_TAG.MANAGE_CONTEST)
@Role(UserRole.Admin)
@Controller('admin/contests')
@SerializeOptions({ groups: [SerialGroup.Admin] })
export class ManageContestController {
  constructor(private readonly contestService: ContestService) {}

  @Get('')
  async filter(
    @Query() query: ContestFilter,
  ): Response<IContest[]> {
    return this.contestService.filter(query);
  }

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) contestId: number,
    @CurrentUser() user?: IManager,
  ): Response<IContest> {
    const contest = await this.contestService.findByIdOrFail(contestId);
    contest.checkReadAuthority(user);

    return ResponseDTO.ok(contest);
  }

  @Post('')
  async create(
    @Body() dto: ContestDTO,
    @CurrentUser() user: IManager,
  ): Response<number> {
    const contest = await this.contestService.create({
      ...dto,
      createdBy: User.withId(user.getIdentifier()),
    });

    return ResponseDTO.ok(contest.id);
  }

  @Put(':id')
  async update(
    @Body() dto: ContestDTO,
    @Param('id', ParseIntPipe) contestId: number,
    @CurrentUser() user: IManager,
  ): Response {
    const contest = await this.contestService.findByIdOrFail(contestId);
    contest.checkWriteAuthority(user);

    await this.contestService.update(contestId, dto);
    return ResponseDTO.ok();
  }

  @Delete(':id')
  async delete(
    @Param('id', ParseIntPipe) contestId: number,
    @CurrentUser() user: IManager,
  ): Response {
    const contest = await this.contestService.findByIdOrFail(contestId);
    contest.checkWriteAuthority(user);

    await this.contestService.remove(contestId);
    return ResponseDTO.ok();
  }
}
