import { BaseModel } from '../../base/base.model';
import { IContest } from '../interfaces/contest.interface';
import { User } from '../../user/serializers/user.serializer';
import { ContestRule } from '../constants/contest-rule';
import { Expose, Type } from 'class-transformer';
import {
  AuthorityException,
  AuthorityType,
  IManageable,
} from '../../../common/interfaces/manageable.interface';
import { IManager } from '../../../common/interfaces/role.interface';
import { UserRole } from '../../user/constants/user-role';
import { anyTruthy } from '../../../common/helpers/array.helper';
import { IUser } from '../../user/interfaces/user.interface';
import { Pick } from '../../../common/decorators/serializers/pick.decorator';
import { SerialGroup } from '../../../common/constants/serial-group';

export class Contest extends BaseModel implements IContest, IManageable {
  closeRankAt: Date;

  updatedRankAt: Date;

  description: string;

  endAt: Date;

  id: number;

  image: string;

  name: string;

  @Expose({ groups: [SerialGroup.Admin] })
  password: string;

  rule: ContestRule;

  startAt: Date;

  joinableGroups: string[];

  numberOfContestants?: number;

  joined?: boolean;

  joinable?: boolean;

  @Type(() => User)
  @Pick('id', 'username', 'avatar', 'fullName')
  createdBy: IUser;

  createdById: IUser['id'];

  @Expose()
  get isPrivate(): boolean {
    return this.password.length > 0;
  }

  constructor(partial?: Partial<Contest>) {
    super();
    Object.assign(this, partial);
  }

  checkReadAuthority(manager: IManager): void {
    if (
      !anyTruthy(
        manager.hasRole(UserRole.Admin),
        this.createdById === manager.getIdentifier(),
      )
    )
      throw AuthorityException.of(Contest.name, AuthorityType.read);
  }

  checkWriteAuthority(manager: IManager): void {
    if (
      !anyTruthy(
        manager.hasRole(UserRole.Admin),
        this.createdById === manager.getIdentifier(),
      )
    )
      throw AuthorityException.of(Contest.name, AuthorityType.write);
  }

  withId(id: number): Contest {
    return new Contest({ id });
  }
}
