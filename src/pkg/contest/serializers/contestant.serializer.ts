import { BaseModel } from '../../base/base.model';
import { IContestant } from '../interfaces/contestant.interface';
import { IContest } from '../interfaces/contest.interface';
import { IUser } from '../../user/interfaces/user.interface';
import { Pick } from '../../../common/decorators/serializers/pick.decorator';

export class Contestant extends BaseModel implements IContestant {
  @Pick('id', 'name', 'image', 'rule', 'startAt', 'endAt', 'numberOfContestants')
  contest?: IContest;

  contestId: IContest['id'];

  @Pick('id', 'username', 'avatar', 'fullName')
  user: IUser;

  userId: IUser['id'];

  constructor(partial?: Partial<IContestant>) {
    super();
    Object.assign(this, partial);
  }
}
