import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class APILoggerMiddleware implements NestMiddleware {
  private readonly logger: Logger = new Logger(APILoggerMiddleware.name);

  use(request: Request, response: Response, next: NextFunction): void {
    const startTime = Date.now();

    response.on('close', () => {
      const { method, originalUrl: url } = request;
      const ip = request.get('x-real-ip') ?? '';
      const userAgent = request.get('user-agent') ?? '-';
      const { statusCode } = response;
      const contentLength = response.get('content-length');
      const duration = Date.now() - startTime;
      const userId = (request as any)?.user?.id ?? '-';
      this.logger.debug(
        `${ip} ${method} ${statusCode} ${contentLength} ${url} "${userAgent}" ${userId} ${duration}ms`,
      );
    });

    next();
  }
}
