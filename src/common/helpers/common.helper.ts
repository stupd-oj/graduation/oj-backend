export function isBetween<T>(value: T, min: T, max: T): boolean {
  return min <= value && value <= max;
}
