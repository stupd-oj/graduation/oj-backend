export const allTruthy = (...args: any[]) => args.every(Boolean);

export const anyTruthy = (...args: any[]) => args.some(Boolean);
