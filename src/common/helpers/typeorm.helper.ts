import { BaseFilter } from "../../pkg/base/dto/filter";
import { FindManyOptions } from "typeorm";

export function exactFindOptions<T extends any>(filter: BaseFilter): FindManyOptions<T> {
  const findOptions: FindManyOptions<T> = {
    skip: filter.offset,
    take: filter.limit,
    order: {},
    where: {},
    select: filter?.select?.split(',')?.map((field) => field.trim()) as any
  };
  for (const sort of filter.sort) {
    findOptions.order[sort.field] = sort.direction;
  }
  findOptions.where = extractFilterToObject(filter.filter);
  return findOptions;
}

export function mergeWhere<T extends any>(findOptions: any, ...where: any[]): FindManyOptions<T> {
  if (!where) return findOptions;
  if (Array.isArray(findOptions.where)) {
    findOptions.where = [...findOptions.where, ...where];
  } else {
    findOptions.where = [findOptions.where, ...where];
  }
  return findOptions;
}

function extractFilterToObject(filter: string): any {
  if (!filter) return {};
  return {};
}
