import * as moment from 'moment';

function diff(
  from: Date,
  to: Date,
  unitOfTime: moment.unitOfTime.Base,
): number {
  return moment.duration({ from, to }).as(unitOfTime);
}

function plus(
  from: Date | number | string,
  duration: number,
  unitOfTime: moment.unitOfTime.Base,
): Date {
  return moment(from).add(duration, unitOfTime).toDate();
}

const dateHelper = {
  diff,
  plus,
};

export default dateHelper;
