import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { IAmqpConfig } from '../../config/amqp/amqp-config.interface';
import { AmqpConfig } from '../../config/amqp/amqp.config';
import { RmqOptions, Transport } from '@nestjs/microservices';
import { AmqpQueue } from '../../providers/amqp/constants/queue';
import { API_TAG } from '../constants/api-tag';

export const initAPIDocs = (app: INestApplication): void => {
  /* Swagger config */
  const swaggerConfigBuilder = new DocumentBuilder().addBearerAuth();
  for (const value of Object.values(API_TAG)) {
    swaggerConfigBuilder.addTag(value);
  }
  const swaggerDocument = SwaggerModule.createDocument(
    app,
    swaggerConfigBuilder.build(),
  );
  SwaggerModule.setup('/docs', app, swaggerDocument);
};

export const initMicroservices = async (
  app: INestApplication,
): Promise<void> => {
  const amqpConfig: IAmqpConfig = app.get(AmqpConfig);
  app.connectMicroservice<RmqOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [amqpConfig.uri],
      queue: AmqpQueue.API_SERVICE,
      queueOptions: { durable: true },
      prefetchCount: 1,
    },
  });
  await app.startAllMicroservices();
};
