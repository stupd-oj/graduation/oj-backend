import { IRedisConfig } from '../../config/redis/redis-config.interface';
import { createClient, RedisClientType } from 'redis';
import { Logger } from '@nestjs/common';

const logger = new Logger('Redis');

export const createRedisClient = (options: IRedisConfig): RedisClientType => {
  const client: RedisClientType = createClient(options);
  client.on('error', logger.error);
  return client;
};
