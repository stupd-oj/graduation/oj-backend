import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { HttpException } from '../exceptions/http.exception';
import { IManager } from '../interfaces/role.interface';
import { Metadata } from '../constants/metadata';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') implements CanActivate {
  constructor(private readonly reflector: Reflector) {
    super();
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return super.canActivate(context);
  }

  handleRequest(err, user, info, context) {
    const isPublic =
      this.reflector.get<boolean>(Metadata.IS_PUBLIC, context.getHandler()) ??
      this.reflector.get<boolean>(Metadata.IS_PUBLIC, context.getClass());

    if (!isPublic && (err || !(user as IManager)?.isAuthenticated?.())) {
      throw HttpException.unauthorized();
    }
    return user;
  }
}
