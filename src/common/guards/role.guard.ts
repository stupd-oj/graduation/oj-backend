import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { isNil } from 'lodash';
import { Metadata } from '../constants/metadata';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requiredRole: number =
      this.reflector.get<number>(Metadata.ROLE, context.getHandler()) ??
      this.reflector.get<number>(Metadata.ROLE, context.getClass());
    const request = context.switchToHttp().getRequest();
    return isNil(requiredRole) || request?.user?.hasRole(requiredRole);
  }
}
