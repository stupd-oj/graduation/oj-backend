import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  Logger,
  HttpException as NestHttpException,
  HttpStatus,
} from '@nestjs/common';
import { ResponseDTO } from '../../pkg/base/dto/response';
import {
  Response as ExpressResponse,
  Request as ExpressRequest,
} from 'express';
import { HttpException } from './http.exception';
import { PreDefinedError } from '../constants/pre-defined-error';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(HttpExceptionFilter.name);
  catch(e: NestHttpException | unknown, host: ArgumentsHost): void {
    this.logger.error(e);
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<ExpressResponse>();
    const request = ctx.getRequest<ExpressRequest>();
    const meta = { path: request.url };

    if (e instanceof NestHttpException) {
      response
        .status(e.getStatus())
        .json(ResponseDTO.error(e.getResponse(), meta));
    } else {
      response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json(
          ResponseDTO.error(
            HttpException.internalServerError(PreDefinedError.UNKNOWN),
            meta,
          ),
        );
    }
  }
}
