import { HttpException as NestHttpException, HttpStatus } from '@nestjs/common';

export interface IError {
  code?: string;
  message: string;
}

export class HttpException extends NestHttpException {
  public static badRequest(
    error: IError = { message: 'Bad request!', code: null },
  ): HttpException {
    return new HttpException(error, HttpStatus.BAD_REQUEST);
  }

  public static forbidden(
    error: IError = {
      message: 'You not have permission!',
      code: null,
    },
  ): HttpException {
    return new HttpException(error, HttpStatus.FORBIDDEN);
  }

  public static notFound(
    error: IError = {
      message: 'Resources not found!',
      code: null,
    },
  ): HttpException {
    return new HttpException(error, HttpStatus.NOT_FOUND);
  }

  public static internalServerError(
    error: IError = {
      message: 'Internal server error, please try later!',
      code: null,
    },
  ): HttpException {
    return new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public static unauthorized(
    error: IError = { message: 'Unauthorized!', code: null },
  ): HttpException {
    return new HttpException(error, HttpStatus.UNAUTHORIZED);
  }

  public static rateLimited(
    error: IError = { message: 'Rate limit exceeded!', code: null },
  ): HttpException {
    return new HttpException(error, HttpStatus.TOO_MANY_REQUESTS);
  }
}
