import { IManager } from './role.interface';
import { HttpException } from '../exceptions/http.exception';
import { PreDefinedError } from '../constants/pre-defined-error';

// Use under manager endpoint: CREATE/READ/UPDATE/DELETE
export interface IManageable {
  checkReadAuthority(manager: IManager): void;
  checkWriteAuthority(manager: IManager): void;
}

export enum AuthorityType {
  read = 'read',
  write = 'write',
}

export class AuthorityException {
  static of(target: string, authorityType: AuthorityType) {
    return HttpException.forbidden(
      PreDefinedError.AUTHORITY_ERROR(target, authorityType),
    );
  }
}
