export interface IManager {
  isAuthenticated(): boolean;
  getIdentifier(): any;
  hasRole(role: any): boolean;
}
