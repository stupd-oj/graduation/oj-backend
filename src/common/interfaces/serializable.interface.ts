export interface Serializable<F, T extends F = F> {
  toSerializer(plain: F): T;
  toSerializerMany(plain: F[]): T[];
}
