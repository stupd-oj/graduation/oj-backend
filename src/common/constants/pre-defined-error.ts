export type Error = {
  code: string;
  message: string;
};

export class PreDefinedError {
  static UNKNOWN: Error = {
    code: 'unhandled',
    message: 'Unhandled error!',
  };
  static AUTH_INCORRECT: Error = {
    code: 'auth_incorrect',
    message: 'Username and password are incorrect',
  };
  static REGISTER_EMAIL_EXISTED: Error = {
    code: 'register_identifier_existed',
    message: 'Username or email has been used',
  };
  static REGISTER_USERNAME_EXISTED: Error = {
    code: 'register_username_existed',
    message: 'Username is existed',
  };
  static ACCOUNT_UNVERIFIED: Error = {
    code: 'account_unverified',
    message: 'Account is not verified, please check your mailbox',
  };
  static ACCOUNT_VERIFIED: Error = {
    code: 'account_verified',
    message: 'Account is already verified',
  };
  static OTP_INCORRECT: Error = {
    code: 'otp_incorrect',
    message: 'OTP is incorrect',
  };
  static CONTEST_INCORRECT_PASSWORD: Error = {
    code: 'contest_incorrect_password',
    message: 'Contest password is incorrect',
  };
  static CONTEST_ALREADY_CONTESTANT: Error = {
    code: 'contest_already_contestant',
    message: 'You are already a contestant',
  };
  static CONTEST_ENDED: Error = {
    code: 'contest_ended',
    message: 'Contest is ended',
  };
  static PROBLEM_NOT_START: Error = {
    code: 'problem_not_start',
    message: 'Chưa đến thời gian làm bài',
  };
  static USER_NOT_FOUND: Error = {
    code: 'user_not_found',
    message: 'User not found',
  };
  static OLD_PASSWORD_INCORRECT: Error = {
    code: 'old_password_incorrect',
    message: 'Old password is incorrect',
  };

  static REFRESH_TOKEN_INCORRECT: Error = {
    code: 'refresh_token_incorrect',
    message: 'Phiên đăng nhập của bạn đã hết hạn',
  };
  static CONTEST_NOT_FOUND: Error = {
    code: 'contest_not_found',
    message: 'Contest not found',
  };
  static PROBLEM_NOT_FOUND: Error = {
    code: 'problem_not_found',
    message: 'Problem not found',
  };
  static SUBMISSION_NOT_FOUND: Error = {
    code: 'submission_not_found',
    message: 'Submission not found',
  };
  static SUBMISSION_DENIED: Error = {
    code: 'submission_denied',
    message: 'You have not permissions to submit',
  };
  static NOT_IMPLEMENTED: Error = {
    code: 'not_implemented',
    message: 'This feature is not implemented yet',
  };
  static OTP_RATE_LIMIT_EXCEEDED: Error = {
    code: 'otp_rate_limit_exceeded',
    message: 'Please wait for sending OTP again',
  };
  static CONTEST_IS_PRIVATE: Error = {
    code: 'contest_is_private',
    message: 'You do not permission to view this contest',
  };
  static AUTHORITY_ERROR = (target: string, action: string): Error => ({
    code: `authority_error`,
    message: `You do not permission to ${action} this ${target}`,
  });
  static SOCIAL_LOGIN_ERROR = (message: string): Error => ({
    code: 'social_login_error',
    message: `Login failed: ${message}`,
  });
  static NUMBER_PARSED_INVALID = (fieldName: string): Error => ({
    code: 'number_parsed_invalid',
    message: `${fieldName} not a valid number`,
  });
  static CANNOT_REMOVE_STARTED_CONTEST: Error = {
    code: 'cannot_remove_started_contest',
    message: 'Cannot remove started contest',
  };
  static UNAUTHORIZED: Error = {
    code: 'unauthorized',
    message: 'Unauthorized!',
  };
}
