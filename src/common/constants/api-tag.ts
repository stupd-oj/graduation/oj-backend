export const API_TAG = {
  USER: 'User',
  MANAGE_USER: 'Manage User',
  CONTEST: 'Contest',
  MANAGE_CONTEST: 'Manage Contest',
  PROBLEM: 'Problem',
  MANAGE_PROBLEM: 'Manage Problem',
  SUBMISSION: 'Submission',
  MANAGE_SUBMISSION: 'Manage Submission',
};
