import { Transform } from 'class-transformer';
import { pick } from 'lodash';

export const Pick = (...fields: string[]) =>
  Transform(({ value }) => pick(value, ...fields));
