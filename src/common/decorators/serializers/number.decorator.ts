import { Transform, TransformOptions } from 'class-transformer';
import { PreDefinedError } from '../../constants/pre-defined-error';
import { HttpException } from '../../exceptions/http.exception';

export const Number = ({
  acceptNaN,
  ...options
}: TransformOptions & { acceptNaN?: boolean } = {}) =>
  Transform(({ value, key }) => {
    const parsedValue = parseInt(value);
    if (!acceptNaN && isNaN(parsedValue))
      throw HttpException.badRequest(
        PreDefinedError.NUMBER_PARSED_INVALID(key),
      );
    return parsedValue;
  }, options);
