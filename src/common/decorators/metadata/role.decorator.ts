import { SetMetadata } from '@nestjs/common';
import { UserRole } from '../../../pkg/user/constants/user-role';
import { Metadata } from '../../constants/metadata';

export const Role = (role: UserRole) => SetMetadata(Metadata.ROLE, role);
