import {
  IsNumber as BaseIsNumber,
  IsNumberOptions as BaseIsNumberOptions,
  Max,
  Min,
} from 'class-validator';
import { applyDecorators } from '@nestjs/common';

interface IsNumberOptions extends BaseIsNumberOptions {
  min?: number;
  max?: number;
  defaultValue?: number;
}

export const IsNumber = ({ max, min, ...options }: IsNumberOptions = {}) => {
  const decorators = [
    BaseIsNumber(options),
    typeof min === 'number' ? Min(min) : null,
    typeof max === 'number' ? Max(max) : null,
  ];
  return applyDecorators(...decorators.filter((d) => !!d));
};
