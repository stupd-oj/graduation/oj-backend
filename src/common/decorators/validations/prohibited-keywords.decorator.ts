import { registerDecorator, ValidationArguments } from 'class-validator';

export const ProhibitedKeywords =
  (...keywords: string[]) =>
  (object: unknown, propertyName: string) => {
    registerDecorator({
      name: 'prohibitedKeywords',
      target: object.constructor,
      propertyName: propertyName,
      constraints: keywords,
      options: {
        message: `Value cannot contain [${keywords.join(', ')}]`,
      },
      validator: {
        validate: (value: string, { constraints }: ValidationArguments) =>
          !constraints.some((e) => value?.includes(e)),
      },
    });
  };
