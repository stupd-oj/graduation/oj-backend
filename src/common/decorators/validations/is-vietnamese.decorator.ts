import { registerDecorator } from 'class-validator';

export const IsVietnamese = () => (object: unknown, propertyName: string) => {
  registerDecorator({
    name: 'isVietnamese',
    target: object.constructor,
    propertyName: propertyName,
    constraints: [],
    options: {
      message: `Value cannot contain special characters`,
    },
    validator: {
      validate,
    },
  });
};

function validate(string: string) {
  function removeAscent(str: string) {
    if (str === null || str === undefined) return str;
    str = str.toLowerCase();
    str = str.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a');
    str = str.replace(/[èéẹẻẽêềếệểễ]/g, 'e');
    str = str.replace(/[ìíịỉĩ]/g, 'i');
    str = str.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o');
    str = str.replace(/[ùúụủũưừứựửữ]/g, 'u');
    str = str.replace(/[ỳýỵỷỹ]/g, 'y');
    str = str.replace(/đ/g, 'd');
    return str;
  }
  const re = /^[a-zA-Z0-9\[\]\s]+$/g;
  return re.test(removeAscent(string));
}
