import {
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  RelationId,
} from 'typeorm';
import { BaseEntity } from '../../pkg/base/base.entity';
import { ContestEntity } from './contest.entity';
import { IContestant } from '../../pkg/contest/interfaces/contestant.interface';
import { UserEntity } from './user.entity';

@Entity('contestant')
export class ContestantEntity extends BaseEntity implements IContestant {
  @ManyToOne(() => ContestEntity, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({
    name: 'contest_id',
  })
  contest?: ContestEntity;

  @PrimaryColumn({ name: 'contest_id', type: 'int' })
  @RelationId((e: ContestantEntity) => e.contest)
  contestId!: ContestEntity['id'];

  @ManyToOne(() => UserEntity, {
    eager: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({
    name: 'user_id',
  })
  user!: UserEntity;

  @PrimaryColumn({ name: 'user_id', type: 'int' })
  @RelationId((e: ContestantEntity) => e.user)
  userId!: UserEntity['id'];
}
