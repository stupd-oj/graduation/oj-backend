import {
  Column,
  Entity,
  Index,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import * as bcrypt from 'bcryptjs';
import { BaseEntity } from '../../pkg/base/base.entity';
import { IUser } from '../../pkg/user/interfaces/user.interface';
import { UserRole } from '../../pkg/user/constants/user-role';

@Entity('user')
export class UserEntity extends BaseEntity implements IUser {
  @PrimaryGeneratedColumn()
  id!: number;

  @Index({ unique: true })
  @Column({
    type: 'varchar',
    name: 'username',
    length: 30,
    nullable: false,
  })
  username!: string;

  @Exclude()
  @Column({
    type: 'varchar',
    name: 'password',
    length: 255,
    nullable: false,
  })
  private password!: string;

  @Index({ unique: true })
  @Column({
    type: 'varchar',
    name: 'email',
    length: 255,
    nullable: false,
  })
  email!: string;

  @Column({
    type: 'smallint',
    name: 'role',
    default: UserRole.User,
  })
  role!: number;

  @Column({
    type: 'varchar',
    name: 'phone_number',
    length: 15,
    nullable: true,
  })
  phoneNumber?: string;

  @Column({
    type: 'varchar',
    name: 'full_name',
    length: 30,
    nullable: false,
  })
  fullName!: string;

  @Column({
    type: 'varchar',
    name: 'mood',
    length: 255,
    default: '',
  })
  mood?: string;

  @Column({
    type: 'boolean',
    name: 'gender',
    default: null,
  }) // male are false, female are always true
  gender?: boolean;

  @Column({
    type: 'varchar',
    name: 'avatar',
    default: null,
  })
  avatar?: string;

  @Column({
    type: 'date',
    name: 'birthday',
    nullable: true,
  })
  birthday?: Date;

  @Column({
    type: 'varchar',
    name: 'school_name',
    length: 255,
    default: '',
  })
  schoolName?: string;

  @Column({
    type: 'varchar',
    name: 'class_name',
    length: 30,
    default: '',
  })
  className?: string;

  @Column({
    type: 'varchar',
    name: 'student_id',
    length: 30,
    default: '',
  })
  studentId?: string;

  @Column({
    type: 'boolean',
    name: 'is_verified',
    default: false,
  })
  isVerified!: boolean;

  public setPassword(password: string) {
    this.password = bcrypt.hashSync(password, 12);
  }

  public comparePassword(password: string): boolean {
    return bcrypt.compareSync(password, this.password);
  }
}
