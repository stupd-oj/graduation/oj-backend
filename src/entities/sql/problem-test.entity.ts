import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { BaseEntity } from '../../pkg/base/base.entity';
import { ProblemEntity } from './problem.entity';
import { IProblemTest } from '../../pkg/problem/interfaces/problem-test.interface';

@Entity('problem_test')
export class ProblemTestEntity extends BaseEntity implements IProblemTest {
  @PrimaryGeneratedColumn('uuid')
  uuid!: string;

  @Column({
    type: 'int',
    name: 'number',
  })
  number!: number;

  @Column({
    type: 'varchar',
    name: 'input_uri',
  })
  inputUri!: string;

  @Column({
    type: 'varchar',
    name: 'output_uri',
  })
  outputUri!: string;

  @Column({
    type: 'boolean',
    name: 'active',
  })
  active!: boolean;

  @ManyToOne(() => ProblemEntity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'problem_id' })
  problem?: ProblemEntity;

  @Index()
  @Column({ name: 'problem_id', type: 'int' })
  @RelationId((e: ProblemTestEntity) => e.problem)
  problemId!: ProblemEntity['id'];
}
