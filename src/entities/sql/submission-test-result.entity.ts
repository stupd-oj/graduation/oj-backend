import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  RelationId,
} from 'typeorm';
import { SubmissionEntity } from './submission.entity';
import { ISubmissionTestResult } from '../../pkg/submission/interfaces/submission-test-result.interface';

@Entity('submission_test_result')
export class SubmissionTestResultEntity implements ISubmissionTestResult {
  @Column({
    type: 'varchar',
    name: 'status',
  })
  result!: string;

  @Column({
    type: 'int',
    name: 'memory',
    default: null,
  })
  memory?: number;

  @Column({
    type: 'float',
    name: 'time',
    default: null,
  })
  time?: number;

  @PrimaryColumn({ name: 'test_uuid', type: 'varchar' })
  testUuid!: string;

  @ManyToOne(() => SubmissionEntity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'submission_id' })
  submission?: SubmissionEntity;

  @PrimaryColumn({ name: 'submission_id', type: 'int' })
  @RelationId((e: SubmissionTestResultEntity) => e.submission)
  submissionId!: SubmissionEntity['id'];
}
