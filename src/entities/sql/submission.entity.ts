import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { ProblemEntity } from './problem.entity';
import { UserEntity } from './user.entity';
import { BaseEntity } from '../../pkg/base/base.entity';
import { ISubmission } from '../../pkg/submission/interfaces/submission.interface';
import { SubmissionTestResultEntity } from './submission-test-result.entity';
import { SubmissionStatus } from '../../pkg/submission/constants/submission-status';

@Entity('submission')
export class SubmissionEntity extends BaseEntity implements ISubmission {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'text',
    name: 'source',
  })
  source!: string;

  @Column({
    type: 'varchar',
    name: 'language',
    length: 11,
  })
  language!: string;

  @Column({
    type: 'varchar',
    name: 'status',
    default: SubmissionStatus.IQ,
  })
  status!: string;

  @Column({
    type: 'float',
    name: 'time',
    default: null,
  })
  time?: number;

  @Column({
    type: 'int',
    name: 'memory',
    default: null,
  })
  memory?: number;

  @Column({
    type: 'int',
    name: 'score',
    default: null,
  })
  score?: number;

  @Column({
    type: 'boolean',
    name: 'shared',
    default: false,
  })
  shared!: boolean;

  @Index()
  @ManyToOne(() => ProblemEntity, {
    eager: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'problem_id' })
  problem!: ProblemEntity;

  @Column({ name: 'problem_id', type: 'int', nullable: false, select: false })
  @RelationId((e: SubmissionEntity) => e.problem)
  problemId!: ProblemEntity['id'];

  @Index()
  @ManyToOne(() => UserEntity, { eager: true })
  @JoinColumn({ name: 'created_by' })
  createdBy!: UserEntity;

  @RelationId((e: SubmissionEntity) => e.createdBy)
  createdById!: UserEntity['id'];

  @OneToMany(() => SubmissionTestResultEntity, (e) => e.submission, {
    cascade: true,
  })
  testResults?: SubmissionTestResultEntity[];
}
