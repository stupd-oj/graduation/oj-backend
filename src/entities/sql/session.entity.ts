import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { UserEntity } from './user.entity';
import { BaseEntity } from '../../pkg/base/base.entity';
import { ISession } from '../../pkg/authentication/interfaces/session.interface';

@Entity('session')
export class SessionEntity extends BaseEntity implements ISession {
  @PrimaryGeneratedColumn('uuid')
  uuid!: string;

  @Column({
    type: 'timestamp',
    name: 'last_active',
  })
  lastActive!: Date;

  @Column({
    type: 'varchar',
    name: 'refresh_token',
  })
  refreshToken!: string;

  @Column({
    type: 'varchar',
    name: 'user_agent',
  })
  userAgent!: string;

  @Column({
    type: 'varchar',
    name: 'ip_address',
  })
  ipAddress!: string;

  @Column({
    type: 'timestamp',
    name: 'expire_at',
  })
  expireAt!: Date;

  @ManyToOne(() => UserEntity, {
    nullable: false,
    eager: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({
    name: 'user_id',
  })
  user!: UserEntity;

  @RelationId((e: SessionEntity) => e.user)
  userId!: UserEntity['id'];
}
