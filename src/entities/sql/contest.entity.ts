import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  JoinColumn,
  RelationId,
  OneToMany,
} from 'typeorm';
import { UserEntity } from './user.entity';
import { IContest } from '../../pkg/contest/interfaces/contest.interface';
import { BaseEntity } from '../../pkg/base/base.entity';
import { ContestantEntity } from './contestant.entity';

@Entity('contest')
export class ContestEntity extends BaseEntity implements IContest {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'varchar',
    name: 'name',
    length: 255,
    nullable: false,
  })
  name!: string;

  @Column({
    type: 'text',
    name: 'description',
  })
  description!: string;

  @Column({
    type: 'varchar',
    name: 'image',
    default: null,
  })
  image?: string;

  @Column({
    type: 'varchar',
    length: 3,
    name: 'rule',
  })
  rule!: string;

  @Column({
    type: 'timestamp',
    name: 'start_at',
  })
  startAt!: Date;

  @Column({
    type: 'timestamp',
    name: 'end_at',
  })
  endAt!: Date;

  @Column({
    type: 'timestamp',
    name: 'close_rank_at',
    default: null,
  })
  closeRankAt!: Date;

  @Column({
    type: 'timestamp',
    name: 'updated_rank_at',
    default: null,
  })
  updatedRankAt?: Date;

  @Column({
    type: 'varchar',
    name: 'password',
    length: 255,
    default: '',
  })
  password!: string;

  // Relations

  @ManyToOne(() => UserEntity, {
    eager: true,
    nullable: true,
  })
  @JoinColumn({
    name: 'created_by',
  })
  createdBy!: UserEntity;

  @Column({ name: 'created_by', type: 'int' })
  @RelationId((e: ContestEntity) => e.createdBy)
  createdById!: UserEntity['id'];

  @OneToMany(() => ContestantEntity, (o) => o.contest)
  contestants?: ContestantEntity[];

  joined?: boolean;
  numberOfContestants?: number;
}
