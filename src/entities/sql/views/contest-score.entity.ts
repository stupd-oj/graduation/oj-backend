import { ViewColumn, ViewEntity } from 'typeorm';
import { IContestScore } from '../../../pkg/ranking/interfaces/contest-score.interface';
import { IProblemHighScore } from '../../../pkg/ranking/interfaces/problem-high-score.interface';

@ViewEntity({
  name: 'contest_score_view',
  expression: `
      SELECT c.id                as contest_id,
             hs.user_id          as user_id,
             sum(hs.score)       as score,
             sum(case
                     when c.rule = 'ACM' then (case when hs.solved then hs.time else 0 end +
                                               hs.solved::int * (20 * (hs.tried - 1)))
                     else 0 end) as time,
       JSONB_AGG(
               JSONB_BUILD_OBJECT(
                       'problem_id', hs.problem_id,
                       'score', COALESCE(hs.score, 0),
                       'time', COALESCE(hs.time, 0),
                       'tried', COALESCE(hs.tried, 0),
                       'solved', COALESCE(hs.solved, false)
               )
       )                   as records
      FROM contest c
          LEFT JOIN problem p on c.id = p.contest_id
          LEFT JOIN problem_high_score hs on p.id = hs.problem_id
      WHERE NOT hs.user_id IS NULL
      GROUP BY hs.user_id, c.id
  `,
})
export class ContestScoreEntity implements IContestScore {
  @ViewColumn({ name: 'contest_id' })
  contestId: number;

  @ViewColumn({ name: 'user_id' })
  userId: number;

  @ViewColumn({ name: 'score' })
  score: number;

  @ViewColumn({ name: 'time' })
  time: number;

  @ViewColumn({ name: 'records' })
  records: Array<IProblemHighScore>;
}
