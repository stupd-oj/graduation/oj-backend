import { BaseEntity, ViewColumn, ViewEntity } from 'typeorm';
import { IProfile } from '../../../pkg/user/interfaces/profile.interface';

@ViewEntity({
  name: 'profile',
  // language=SQL
  expression: `WITH submissions AS (SELECT count(s.id) AS total_submissions, s.created_by AS user_id
                                    FROM "submission" s
                                    GROUP BY s.created_by),
                    solved AS (SELECT count(phs.problem_id) AS total_solved, phs.user_id AS user_id
                               FROM "problem_high_score" phs
                               WHERE phs.solved
                               GROUP BY phs.user_id),
                    total_contests AS (SELECT count(c.contest_id) AS total_contests, c.user_id
                                       FROM "contestant" c
                                       GROUP BY c.user_id)
               SELECT u.id                             AS id,
                      u.username                       AS username,
                      u.full_name                      AS full_name,
                      u.avatar                         AS avatar,
                      u.gender                         AS gender,
                      u.mood                           AS mood,
                      coalesce(s.total_submissions, 0) AS total_submissions,
                      coalesce(sv.total_solved, 0)     AS total_solved,
                      coalesce(ctt.total_contests, 0)  AS total_contests
               FROM "user" u
                        LEFT JOIN submissions as s ON s.user_id = u.id
                        LEFT JOIN total_contests as ctt ON ctt.user_id = u.id
                        LEFT JOIN solved as sv ON sv.user_id = u.id;`,
  synchronize: true,
})
export class ProfileEntity extends BaseEntity implements IProfile {
  @ViewColumn({ name: 'id' })
  id!: number;

  @ViewColumn({ name: 'username' })
  username!: string;

  @ViewColumn({ name: 'full_name' })
  fullName: string;

  @ViewColumn({ name: 'avatar' })
  avatar: string;

  @ViewColumn({ name: 'gender' })
  gender: boolean;

  @ViewColumn({ name: 'mood' })
  mood: string;

  @ViewColumn({ name: 'total_submissions' })
  totalSubmissions: number;

  @ViewColumn({ name: 'total_solved' })
  totalSolved: number;

  @ViewColumn({ name: 'total_contests' })
  totalContests: number;
}
