import { UserEntity } from './user.entity';
import { ContestEntity } from './contest.entity';
import { ContestantEntity } from './contestant.entity';
import { ProblemEntity } from './problem.entity';
import { ProblemHighScoreEntity } from './problem-high-score.entity';
import { ProblemTestEntity } from './problem-test.entity';
import { ProfileEntity } from './views/profile.entity';
import { SessionEntity } from './session.entity';
import { SubmissionEntity } from './submission.entity';
import { SubmissionTestResultEntity } from './submission-test-result.entity';
import { ContestRankEntity } from './contest-rank.entity';
import { ContestScoreEntity } from './views/contest-score.entity';

const VIEWS = [ProfileEntity, ContestScoreEntity];

export default [
  ...VIEWS,
  ContestEntity,
  ContestantEntity,
  ProblemEntity,
  ProblemHighScoreEntity,
  ProblemTestEntity,
  SessionEntity,
  SubmissionEntity,
  SubmissionTestResultEntity,
  UserEntity,
  ContestRankEntity,
];
