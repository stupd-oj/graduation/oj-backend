import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  RelationId,
} from 'typeorm';
import { SubmissionEntity } from './submission.entity';
import { ProblemEntity } from './problem.entity';
import { UserEntity } from './user.entity';
import { BaseEntity } from '../../pkg/base/base.entity';
import { IProblemHighScore } from '../../pkg/ranking/interfaces/problem-high-score.interface';

@Entity('problem_high_score')
export class ProblemHighScoreEntity
  extends BaseEntity
  implements IProblemHighScore
{
  @Column({
    type: 'int',
    name: 'score',
  })
  score!: number;

  @Column({
    type: 'int',
    name: 'tried',
    default: 1,
  })
  tried!: number;

  @Column({
    type: 'float',
    name: 'time',
    default: 0,
  })
  time!: number;

  @Column({
    type: 'boolean',
    name: 'solved',
    default: false,
  })
  solved!: boolean;

  @Column({
    type: 'int',
    name: 'submission_ref',
    default: null,
  })
  submissionRefId?: SubmissionEntity['id'];

  @ManyToOne(() => ProblemEntity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'problem_id' })
  problem?: ProblemEntity;

  @Index()
  @PrimaryColumn({ name: 'problem_id', type: 'int' })
  @RelationId((e: ProblemHighScoreEntity) => e.problem)
  problemId!: ProblemEntity['id'];

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity;

  @Index()
  @PrimaryColumn({ name: 'user_id', type: 'int' })
  @RelationId((e: ProblemHighScoreEntity) => e.user)
  userId!: UserEntity['id'];
}
