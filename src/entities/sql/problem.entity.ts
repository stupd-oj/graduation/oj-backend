import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { ContestEntity } from './contest.entity';
import { UserEntity } from './user.entity';
import { BaseEntity } from '../../pkg/base/base.entity';
import { IProblem } from '../../pkg/problem/interfaces/problem.interface';
import { ProblemTestEntity } from './problem-test.entity';

@Entity('problem')
@Index(['contest', 'code'], { unique: true })
export class ProblemEntity extends BaseEntity implements IProblem {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    type: 'varchar',
    name: 'code',
    length: 255,
    nullable: false,
  })
  code!: string;

  @Column({
    type: 'varchar',
    name: 'name',
    length: 255,
    nullable: false,
  })
  name!: string;

  @Column({
    type: 'text',
    name: 'description',
  })
  description!: string;

  @Column({
    type: 'text',
    name: 'hint',
    default: null,
  })
  hint!: string;

  @Column({
    type: 'timestamp',
    name: 'start_at',
  })
  startAt!: Date;

  @Column({
    type: 'timestamp',
    name: 'end_at',
  })
  endAt!: Date;

  @Column({
    type: 'float',
    name: 'time_limit',
    default: 1,
  })
  timeLimit!: number;

  @Column({
    type: 'float',
    name: 'memory_limit',
    default: 256,
  })
  memoryLimit!: number;

  @Column({
    type: 'boolean',
    name: 'active',
  })
  active!: boolean;

  @Column({
    type: 'smallint',
    name: 'level',
  })
  level!: number;

  @Column({
    type: 'int',
    name: 'first_solved',
    default: null,
  })
  firstSolved?: number;

  @Column({
    type: 'smallint',
    name: 'multiplier',
    default: 1,
  })
  multiplier!: number;

  @ManyToOne(() => ContestEntity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'contest_id' })
  contest?: ContestEntity;

  @Index()
  @Column({ name: 'contest_id', type: 'int' })
  @RelationId((e: ProblemEntity) => e.contest)
  contestId!: ContestEntity['id'];

  @ManyToOne(() => UserEntity, { eager: true })
  @JoinColumn({ name: 'created_by' })
  createdBy!: UserEntity;

  @Column({ name: 'created_by', type: 'int' })
  @RelationId((e: ProblemEntity) => e.createdBy)
  createdById!: UserEntity['id'];

  @OneToMany(() => ProblemTestEntity, (e) => e.problem, {
    cascade: true,
  })
  tests?: ProblemTestEntity[];

  numberOfSubmissions?: number;
  canSubmit?: boolean;
}
