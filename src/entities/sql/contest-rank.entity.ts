import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  RelationId,
} from 'typeorm';
import { UserEntity } from './user.entity';
import { ContestEntity } from './contest.entity';
import { IContestScore } from '../../pkg/ranking/interfaces/contest-score.interface';
import { IProblemHighScore } from '../../pkg/ranking/interfaces/problem-high-score.interface';

/**
 * Persistent contest ranking
 */
@Entity('contest_rank')
export class ContestRankEntity implements IContestScore {
  @Column({
    type: 'int',
    name: 'score',
  })
  score!: number;

  @Column({
    type: 'int',
    name: 'rank',
  })
  rank?: number;

  @Column({
    type: 'float',
    name: 'time',
    default: 0,
  })
  time!: number;

  @Column({
    type: 'jsonb',
    name: 'records',
  })
  records: Array<IProblemHighScore>;

  @ManyToOne(() => ContestEntity, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'contest_id' })
  contest?: ContestEntity;

  @PrimaryColumn({ name: 'contest_id', type: 'int' })
  @RelationId((e: ContestRankEntity) => e.contest)
  contestId!: ContestEntity['id'];

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user?: UserEntity;

  @PrimaryColumn({ name: 'user_id', type: 'int' })
  @RelationId((e: ContestRankEntity) => e.user)
  userId!: UserEntity['id'];
}
