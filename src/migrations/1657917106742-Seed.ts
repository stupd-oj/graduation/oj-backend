import { MigrationInterface, QueryRunner } from 'typeorm';
import { UserEntity } from '../entities/sql/user.entity';
import { UserRole } from '../pkg/user/constants/user-role';

export class Seed1657917106742 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const adminUser = queryRunner.manager.create(UserEntity, {
      username: 'admin',
      email: 'admin@cannot.recover',
      fullName: 'Administrator',
      isVerified: true,
      role: UserRole.Admin,
    });
    adminUser.setPassword('Admin@123456');
    await queryRunner.manager.save(adminUser);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete(UserEntity, {
      username: 'admin',
    });
  }
}
