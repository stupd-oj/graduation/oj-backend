import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateGetUserScoreFunc1657704818645 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`
        CREATE FUNCTION get_user_score(_contest_id INT, _user_id INT)
            RETURNS table
                (
                "user_id" INT,
                "score" INT,
                "time" INT,
                "records" JSONB
                )
            LANGUAGE sql AS
        $$
        SELECT cs."user_id" AS "user_id",
               cs."score"::INT    AS "score", cs."time"::INT AS "time",
               cs."records" AS "records"
        FROM "contest_score_view" cs
        WHERE cs."contest_id" = _contest_id
          AND cs."user_id" = _user_id
        LIMIT
            1;
        $$;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`DROP FUNCTION IF EXISTS get_user_score`);
  }
}
