import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateGetContestRankFunc1657704783270
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`
        CREATE FUNCTION get_contest_rank(_contest_id INT, _real_time BOOL = true)
            RETURNS table
                (
                "user_id" INT,
                "score" INT,
                "time" DOUBLE PRECISION,
                "records" JSONB,
                "rank" INT
                )
            LANGUAGE plpgsql
        AS
        $$
        BEGIN
            IF
        _real_time THEN
                RETURN QUERY
        SELECT cs."user_id" AS "user_id",
               cs."score"::INT                                 AS "score", cs."time" AS "time",
               cs."records" AS "records",
               RANK()
                               OVER (ORDER BY cs."score" DESC, cs."time")::INT AS "rank"
        FROM "contest_score_view" cs
        WHERE cs."contest_id" = _contest_id;
        RETURN;
        ELSE
                RETURN QUERY
        SELECT cr."user_id" AS "user_id",
               cr."score"   AS "score",
               cr."time"    AS "time",
               cr."records" AS "records",
               cr."rank"    as "rank"
        FROM "contest_rank" cr
        WHERE cr."contest_id" = _contest_id;
        RETURN;
        END IF;
        END;
        $$;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`DROP FUNCTION IF EXISTS get_ranking_score`);

    //language=SQL
    await queryRunner.query(
      `DROP FUNCTION IF EXISTS get_contest_persistence_rank`,
    );
  }
}
