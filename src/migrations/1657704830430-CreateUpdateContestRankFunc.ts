import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUpdateContestRankFunc1657704830430
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`
        CREATE PROCEDURE update_contest_rank(_contest_id INT)
            LANGUAGE sql AS
        $$
        INSERT
        INTO "contest_rank" ("contest_id", "user_id", "rank", "score", "time", "records")
        SELECT _contest_id, cr."user_id", cr."rank", cr."score", cr."time", cr."records"
        FROM get_contest_rank(_contest_id) cr ON CONFLICT ("contest_id", "user_id") DO
        UPDATE SET ("rank", "score", "time", "records") = (EXCLUDED."rank", EXCLUDED."score", EXCLUDED."time", EXCLUDED."records");

        UPDATE contest
        SET "updated_rank_at" = now()
        WHERE "id" = _contest_id;
        $$;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    //language=SQL
    await queryRunner.query(`DROP PROCEDURE IF EXISTS update_contest_rank;
    `);
  }
}
