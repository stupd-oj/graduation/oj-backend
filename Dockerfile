FROM node:16-alpine
ENV NODE_ENV production
WORKDIR /server
COPY ["templates", "/server/templates"]
RUN apk update && apk add --no-cache libstdc++ libgcc curl
COPY ["package.json", "."]
RUN npm install
COPY dist/ /server/app/
CMD [ "app/main" ]
