# CodeIT
### Description
This project belong to Web Team - ITPTIT Club
### Requirement
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/install)
### Installation
**Rename file `example.env` to `.env.docker`
Each service depend on following environment variables:**
##### Database (Mysql 8)
```dotenv
# Database configure
DB_HOST=127.0.0.1
DB_PORT=3366
MYSQL_USER=root
MYSQL_PASSWORD=123456
MYSQL_DATABASE=itptit
```
Run: `docker-compose up database`
##### Event Queue (RabbitMQ)
```dotenv
# Rabitmq configure
RABBITMQ_HOST=127.0.0.1
RABBITMQ_PORT=5678
RABBITMQ_DEFAULT_VHOST=itptit
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=123456
RABBITMQ_QUEUE=CODE_JUDGE
```
Run: `docker-compose up rabbitmq`
##### Backend
```dotenv
# Server configure
SERVER_PORT=18080
SERVER_API_PREFIX=/api/v1

# JWT configure
JWT_SECRET=123456
JWT_ISSUER=ITPTIT
JWT_EXPIRY=30d

# Mail configure
MAIL_HOST=mail_server
MAIL_PORT=587
MAIL_USER=mail_username
MAIL_PASSWORD=mail_password

# Database configure
DB_HOST=127.0.0.1
DB_PORT=3366
MYSQL_USER=root
MYSQL_PASSWORD=123456
MYSQL_DATABASE=itptit

# Rabitmq configure
RABBITMQ_HOST=127.0.0.1
RABBITMQ_PORT=5678
RABBITMQ_DEFAULT_VHOST=itptit
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=123456
RABBITMQ_QUEUE=CODE_JUDGE
```
Run: `docker-compose up backend`

##### Frontend
```dotenv
# Client configure
CLIENT_PORT=4201
```
Run: `docker-compose up frontend`

##### Judge
```dotenv
# Rabitmq configure
RABBITMQ_HOST=127.0.0.1
RABBITMQ_PORT=5678
RABBITMQ_DEFAULT_VHOST=itptit
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=123456
RABBITMQ_QUEUE=CODE_JUDGE
RABBITMQ_PREFETCH=1
```
Run: `docker-compose up judge-services`
##### Run all services: `docker-compose up`
### Stay in touch
- Author: [Thieu Quan Ngoc](https://www.facebook.com/empty215/)
- Email: [ngocthieu245@gmail.com](mail:ngocthieu245@gmail.com)
- Linkedin: [@empty21](https://www.linkedin.com/in/empty21/)
